<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>T&T Sistema</title>
<!-- Bootstrap core CSS-->
<link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<!-- Custom styles for this template-->
<link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
<link href="{{asset('vendor/select2/dist/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('vendor/touchspin/css/touchspin.min.css')}}" rel="stylesheet" />
@yield('css')
</head>

<body class="fixed-nav sticky-footer bg-light sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" id="mainNav">
    <a href="/sistema"> <img style="height:55px;" src="{{asset('img/logo.png')}}" alt=""></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav"  id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Nueva venta">
          <a class="nav-link" href="/sistema">
            <i class="fa fa-fw fa-usd"></i>
            <span class="nav-link-text">Nueva venta</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Nuevo servicio">
        <a class="nav-link" href="{{route('servicio.index')}}">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text">Nuevo servicio</span>
          </a>
        </li>
        <hr>{{-- ----------------------------------------- --}}

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Ingresos en el día">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseIngresoDia" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-cart-arrow-down"></i>
            <span class="nav-link-text">Movimientos en el día</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseIngresoDia">
            <li>
              <a href="{{route('venta.index')}}">Ventas</a>
            </li>
            <li>
            <a href="{{route('servicio.recientes')}}">Servicios</a>
            </li>
          </ul>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Consultar ventas">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseVentas" data-parent="#exampleAccordion">
            <i class="fa fa-shopping-cart"></i>
            <span class="nav-link-text">Consultar ventas</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseVentas">
            <li>
              <a href="{{route('venta.gconsulta')}}">Todas las ventas</a>
            </li>
            <li>
            <a href="{{route('venta.fecha')}}">Ventas por fecha</a>
            </li>
          </ul>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Consultar servicios">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseConsultaServicios" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-cog"></i>
            <span class="nav-link-text">Consultar servicios</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseConsultaServicios">
            <li>
              <a href="{{route('servicio.gconsulta')}}">Todos los servicios</a>
            </li>
            <li>
            <a href="{{route('servicio.fecha')}}">Servicios por fecha</a>
            </li>
          </ul>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Agenda de clientes">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseClientes" data-parent="#exampleAccordion">
              <i class="fa fa-fw fa-address-book"></i>
              <span class="nav-link-text">Agenda de clientes</span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseClientes">
              <li>
                <a href="{{route('Cliente.index')}}">Clientes</a>
              </li>
              <li>
              <a href="{{route('cliente.fecha')}}">Venta a clientes por fecha</a>
              </li>
            </ul>
          </li>
  

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inventario">
        <a class="nav-link" href="{{route('inventario.index')}}">
              <i class="fa fa-fw fa-cubes"></i>
              <span class="nav-link-text">Inventario</span>
            </a>
          </li>

      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">

        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper mt-3">
    <div class="container">

     
        @yield('contenido')

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center ">
          <small>Recarga Tinta&Toner  2018 | Todos los derechos reservados | Sitio por JvrDeveloper</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">¿Listo para irte?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Presiona Logout para cerrar sesion.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary"
            href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
            
            >Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
          </div>
        </div>
      </div>
    </div>
 <!-- Bootstrap core JavaScript-->
 <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
 <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
 <!-- Core plugin JavaScript-->
 <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
 <!-- Custom scripts for all pages-->
 <script src="{{asset('js/sb-admin.min.js')}}"></script>
 <script src="{{asset('vendor/touchspin/js/touchspin.min.js')}}"></script>
 <script src="{{asset('vendor/select2/dist/js/select2.min.js')}}"></script>
 <script src="{{asset('vendor/select2/dist/js/lang/es.js')}}"></script>
 @yield('js')
  </div>
</body>

</html>
