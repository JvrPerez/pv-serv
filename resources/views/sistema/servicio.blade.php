@extends('layouts.sistema')

@section('css')
<link rel="stylesheet" href="{{asset('css/venta.css')}}">
@endsection

@section('js')
<script src="{{asset('js/servicio.js')}}"></script>
@endsection

@section('contenido')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-fw  fa-wrench icon"></i> Nuevo servicio</li>
    </ol>
</nav>
<form id="servForm" action="{{route('servicio.store')}}" method="post">   </form>

<div class="w-100 d-flex" id="clientelist">
    <div class="input-group input-group-sm mb-4 d-flex flex-nowrap ">
        <div class="input-group-prepend d-sm-none">
          <span class="input-group-text" id="inputGroup-sizing-sm">Cliente</span>
        </div>

      
        <select id="select-cliente" form="servForm" required  name="cliente" class="w-75" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
            <option></option>
            @foreach ($cliente as $item)
        <option value="{{$item->id}}">{{$item->nombre.' '.$item->apellido}}</option>    
            @endforeach
            
        </select>
        <a class="ml-2" href="#" data-toggle="modal" data-target="#addClienteModal"><i style="font-size:32px;" class="fa fa-fw fa-plus-circle text-danger" ></i></a>
    </div>
            
</div>

    <div id="detCliente" class="">
            <div class="d-flex  flex-wrap flex-xl-nowrap">
                <div class="input-group input-group-sm d-flex flex-nowrap ml-0 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Correo</span>
                    </div>
                    <input id="detCorreo" class="form-control" disabled type="email" name="correo" id="">
                </div>
        
                <div class="input-group input-group-sm d-flex flex-nowrap mb-3 ml-xl-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Teléfono</span>
                    </div>
                    <input id="detTelefono" class="form-control" disabled type="text" name="telefono" id="">
                </div>
    
                <div class="input-group input-group-sm d-flex flex-nowrap ml-xl-3 mb-xl-3 mb-md-3 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">R.F.C</span>
                    </div>
                    <input id="detRfc" class="form-control" disabled type="text" name="rfc" id="">
                </div>
            </div>
    
            <div class="mt-lg-0 d-flex flex-wrap flex-xl-nowrap">
                <div class="input-group input-group-sm mr-xl-4 mb-3 d-flex flex-nowrap ">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Dirección</span>
                    </div>
                    <input id="detDireccion" class="form-control" disabled type="text" name="direccion" id="">
                </div>
        
                <div class="input-group input-group-sm d-flex flex-nowrap mb-3 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Ciudad</span>
                    </div>
                    <input id="detCiudad" class="form-control" disabled type="text" name="ciudad" id="">
                </div>
            </div>      
        </div>

    <div class="media-sm input-group input-group-sm  mb-4 w-75">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm">Tipo de servicio</span>
            </div>
            <input required form="servForm" id="tipo" class="form-control"  type="text" name="tipo">
    </div>

    <div class="d-flex w-75 mb-4  flex-wrap flex-xl-nowrap flex-lg-nowrap flex-md-nowrap media-sm">
        <div class="input-group input-group-sm d-flex flex-nowrap mr-4 media-sm mb-3 mb-lx-0 mb-lg-0 mb-md-0">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm">Marca</span>
            </div>
            <input form="servForm" required  class="form-control"  type="text" name="marca">
        </div>

        <div class="input-group input-group-sm d-flex flex-nowrap mr-4 media-sm mb-3 mb-lx-0 mb-lg-0 mb-md-0">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm">Modelo</span>
            </div>
            <input required form="servForm" id="modelo" class="form-control" type="text" name="modelo">
        </div>
        <div class="input-group input-group-sm d-flex flex-nowrap ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm">N/S</span>
            </div>
            <input form="servForm" placeholder="(opcional)" id="serial" class="form-control" type="text" name="serial" >
        </div>
    </div>

    <div class="d-flex  flex-wrap flex-xl-nowrap flex-lg-nowrap flex-md-nowrap media-sm  mb-5">
            <div class="input-group input-group-sm d-flex flex-nowrap mr-4 media-sm mb-3 mb-lx-0 mb-lg-0 mb-md-0">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Falla</span>
                </div>
                <textarea required form="servForm" rows="5" class="form-control" name="descripcion" ></textarea>
            </div>
    
            <div class="input-group input-group-sm d-flex flex-nowrap mr-4 media-sm">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Observaciones</span>
                </div>
                <textarea required form="servForm" rows="5"  class="form-control"  name="observaciones"></textarea>
            </div>

        </div>
 
        <button id="registrar" type="button" data-toggle="modal" data-target="#ModalConfirm" class="mb-4 btn btn-success btn-lg btn-block"><i class=" fa fa-fw fa-check-circle"></i>Registrar</button>

        <!-- Modal confirmación-->

        <div class="modal fade" id="ModalConfirm" tabindex="-1" role="dialog" aria-labelledby="ModalConfirm" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">¿Guardar servicio?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer">
                        <button id="addCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button form="servForm" type="submit" id="guardarServ" class="btn btn-primary">Guardar</button>
                      </div>
                </div>
            </div>
        </div>


<!-- Modal  para nuevo cliente -->
<div class="modal fade" id="addClienteModal" tabindex="-1" role="dialog" aria-labelledby="addClienteModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="">Nuevo cliente</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{route('Cliente.store')}}" id="addCliente" method="post">
                        @csrf
                    <input class="form-control mb-3" type="text" name="nombre" placeholder="Cliente/Empresa*" required id="">
                    <div class="form-group d-flex">
                        <input class="form-control mr-3" placeholder="RFC (opcional)" type="text"  pattern="(^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$)" name="rfc" id="">
                        <input class="form-control" placeholder="Correo (opcional)" type="email" name="correo" id="">
                    </div> 
                    <input class="form-control mb-3" placeholder="Direccion (opcional)" type="text" name="direccion" id="">
                    <div class="form-group d-flex">
                        <input class="form-control mr-3" placeholder="Telefono (opcional)" type="text" pattern="(^([\d]{7}|[\d]{10})$)" name="telefono" id="">
                        <input class="form-control" placeholder="Ciudad (opcional)" type="text" name="ciudad" id="">
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
              <button id="addCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" form="addCliente" class="btn btn-primary">Guardar</button>
            </div>
        
          </div>
        </div>
      </div>
@endsection