@extends('layouts.sistema')
@section('css')
    <link rel="stylesheet" href="{{asset('css/venta.css')}}">
@endsection

@section('js')
<script src="{{asset('js/venta.js')}}"></script>
<script src="{{asset('js/tabletojson.js')}}"></script>
@endsection

@section('contenido')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page"><i class="fa fa-fw fa-dollar icon"></i> Generar venta</li>
    </ol>
  </nav>
<form action="" method="post">
        <div class="w-100 d-flex" id="clientelist">
        <div class="input-group input-group-sm mb-4 d-flex flex-nowrap">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-sm">Cliente</span>
        </div>

      
        <select id="select-cliente"  name="cliente" class="" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
            <option></option>
            @foreach ($cliente as $item)
        <option value="{{$item->id}}">{{$item->nombre.' '.$item->apellido}}</option>    
            @endforeach
            
        </select>
        <a class="ml-2" href="#" data-toggle="modal" data-target="#addClienteModal"><i style="font-size:32px;" class="fa fa-fw fa-plus-circle text-danger" ></i></a>
    </div>
        
    </div>

    <div id="detCliente" class="">
            <div class="d-flex  flex-wrap flex-xl-nowrap">
                <div class="input-group input-group-sm d-flex flex-nowrap ml-0 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Correo</span>
                    </div>
                    <input id="detCorreo" class="form-control" disabled type="email" name="correo" id="">
                </div>
        
                <div class="input-group input-group-sm d-flex flex-nowrap mb-3 ml-xl-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Teléfono</span>
                    </div>
                    <input id="detTelefono" class="form-control" disabled type="text" name="telefono" id="">
                </div>
    
                <div class="input-group input-group-sm d-flex flex-nowrap ml-xl-3 mb-xl-3 mb-md-3 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">R.F.C</span>
                    </div>
                    <input id="detRfc" class="form-control" disabled type="text" name="rfc" id="">
                </div>
            </div>
    
            <div class="mt-lg-0 d-flex flex-wrap flex-xl-nowrap">
                <div class="input-group input-group-sm mr-xl-4 mb-3 d-flex flex-nowrap ">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Dirección</span>
                    </div>
                    <input id="detDireccion" class="form-control" disabled type="text" name="direccion" id="">
                </div>
        
                <div class="input-group input-group-sm d-flex flex-nowrap mb-3 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Ciudad</span>
                    </div>
                    <input id="detCiudad" class="form-control" disabled type="text" name="ciudad" id="">
                </div>
            </div>      
        </div>

<div class="d-flex flex-wrap flex-xl-nowrap  flex-md-nowrap  ">

      <div id="invlist" class="input-group input-group-sm mb-3 d-flex flex-nowrap">
        <div class="input-group-prepend">
          <span class="input-group-text" >Producto</span>
        </div>

        {{-- <input id="textarticulo"  type="text" name="articulo" class="form-control" > --}}
        <select id="select-articulo" name="articulo" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
            <option></option>
            @foreach ($productos as $item)
        <option value="{{$item->id}}">{{$item->descripcion}}</option>    
            @endforeach
            
        </select>
       
        {{-- <a class="ml-3" href="#" data-toggle="modal" data-target="#addClienteModal"><i style="font-size:32px;" class="fa fa-fw fa-plus-circle text-success" ></i></a> --}}
        <a class="ml-2" href="#" data-toggle="modal" data-target="#productoModal"><i style="font-size:32px;" class="fa fa-fw fa-plus-circle text-danger" ></i></a>

        </div>

        <div class="input-group input-group-sm mb-3 inp-num1  ml-xl-4 d-flex flex-nowrap">
                <div class="input-group-prepend">
                    <span class="input-group-text" ><i class="fa fa-fw fa-dollar"></i></span>
                </div>
                <input autocomplete="off" placeholder="0.00" id="precio" type="text" class="form-control numeros" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
        </div>



        <div class="input-group input-group-sm mb-3 inp-num2 ml-md-3 mr-4 d-flex flex-nowrap">
            <div class="input-group-prepend">
                <span class="input-group-text" >Cantidad</span>
            </div>
            <input id="cantidad" type="text" name="cantidad" value="" class="form-control text-center cantidad" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
        </div>

        <button onclick="addProducto()" style="height:32px;" type="button"  class="btn btn-success p-0 px-1" ><i  style="font-size:20px;" class="fa fa-fw fa-arrow-circle-right"></i></button>

</div>
<div class="table-responsive">
    <table id="detVenta" class="table text-nowrap venta" >
        <thead class="thead-dark "  >
        <tr>
            <th class="hidden border-0 p-0" style="width:1%;font-size:3px;" scope="col">IN</th>
            <th class="hidden border-0 p-0" style="width:1%;font-size:3px;" scope="col">SV</th>
            <th style="width:50%;" scope="col">Producto</th>
            <th style="width:10%;" scope="col">Cantidad</th>
            <th style="width:15%;" scope="col">Precio</th>
            <th style="width:15%;" scope="col">Importe</th>
            <th style="width:5%;" scope="col"></th>

        </tr>
        </thead>
        <tbody  id="addProducto">

        {{-- <tr>
                <th scope="row">1</th>
                <td class="text-left" >Mark</td>
                <td>
                    <button data-toggle="modal" data-target="#addCantidadModal" type="button" class="btn btn-primary p-1"><span id="tagCantidad" style="font-size:15px;" class="badge">4</span></button>
                </td>
                <td><i class="fa fa-fw fa-dollar"></i>23423423</td>
                <td><i class="fa fa-fw fa-dollar"></i>2423423</td>
                <td><button type="button" class="btn btn-danger p-0 px-1"><i class="fa fa-fw fa-times-circle"></i></button></td>
            </tr> --}}
        </tbody>
    </table>
</div>
    <hr>
    
    <table class="ml-auto mb-5">
            <thead class="thead-dark">
                    <tr>
                        <th  scope="col"></th>
                        <th  scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
        <tbody>
            <div>
            <tr style="font-size:20px;">
                <th scope="row" class="px-2" >Subtotal</th>
                <td id="subtotal"  class="px-2"><i class="fa fa-fw fa-dollar"></i>0.00</td>
            </tr>

            <tr style="font-size:20px;" >
                <th scope="row" class="px-2" >Descuento </th>
                <td id="subtotal"  class="px-2"><input id="descuento" style="width:80px; font-size:20px;" placeholder="0.00" type="text" class="numeros form-control" ></td>
            </tr>

            <tr style="font-size:20px;">
                <th class="px-2" scope="row">Total</th>
                <td id="total"  class="px-2"><i class="fa fa-fw fa-dollar"></i>0.00</td>
            </tr>
        </div>
        </tbody>
    </table>
<button id="btnCobrarModal" type="button" class="btn btn-success btn-lg btn-block mb-4"><i class="fa fa-fw fa-cart-plus mr-2"></i>Cobrar</button>
</form>
<!-- Modal  para nuevo cliente -->
<div class="modal fade" id="addClienteModal" tabindex="-1" role="dialog" aria-labelledby="addClienteModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addClienteModal">Nuevo cliente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('Cliente.store')}}" id="addCliente" method="post">
                    @csrf
                <input class="form-control mb-3" type="text" name="nombre" placeholder="Cliente/Empresa*" required id="">
                <div class="form-group d-flex flex-wrap flex-xl-nowrap flex-md-nowrap">
                    <input class="form-control mr-xl-3 mr-md-3" placeholder="RFC (opcional)" type="text"  pattern="(^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$)" name="rfc" id="">
                    <input class="form-control mt-3 mt-xl-0 mt-md-0" placeholder="Correo (opcional)" type="email" name="correo" id="">
                </div> 
                <input class="form-control mb-3" placeholder="Direccion (opcional)" type="text" name="direccion" id="">
                <div class="form-group d-flex flex-wrap flex-xl-nowrap flex-md-nowrap">
                    <input class="form-control mr-xl-3 mr-md-3" placeholder="Telefono (opcional)" type="text" pattern="(^([\d]{7}|[\d]{10})$)" name="telefono" id="">
                    <input class="form-control mt-3 mt-xl-0 mt-md-0" placeholder="Ciudad (opcional)" type="text" name="ciudad" id="">
                </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button id="addCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" form="addCliente" class="btn btn-primary">Guardar</button>
        </div>
    
      </div>
    </div>
  </div>


  <!-- Modal cantidad-->
<div class="modal fade" id="addCantidadModal" tabindex="-1" role="dialog" aria-labelledby="addCantidadModal" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="addCantidadModal">Cantidad</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="input-group input-group-sm mx-auto inp-num d-flex flex-nowrap">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Cantidad</span>
                    </div>
                    <input id="modCantidad" type="text" name="cantidad" value="" class="cantidad form-control text-center" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
              <button onclick="modCantidad()" type="button" class="btn btn-primary  btn-sm">Cambiar</button>
            </div>
          </div>
        </div>
      </div>


      <!-- Modal agregar-->
<div class="modal fade" id="productoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Agregar</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link active" id="nav-producto-tab" data-toggle="tab" href="#nav-producto" role="tab" aria-controls="nav-home" aria-selected="true">Nuevo Producto</a>
                      <a class="nav-item nav-link" id="nav-servicio-tab" data-toggle="tab" href="#nav-servicio" role="tab" aria-controls="nav-profile" aria-selected="false">Cobrar un servicio</a>

                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-producto" role="tabpanel" aria-labelledby="nav-home-tab">

                    <form action="{{route('inventario.store')}}" method="POST" id="InveForm">
                        <div class="mt-4"> 
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="inputGroup-sizing-sm">Descripción</span>
                                </div>
                                <input required type="text" class="form-control" name="descripcion" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                            </div>

                            <div class="d-flex flex-wrap  flex-md-nowrap flex-xl-nowrap">
                                <div class="input-group input-group-sm mb-3 mr-md-3 mr-xl-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-fw fa-dollar"></i> Compra</span>
                                    </div>
                                    <input type="text" class="form-control numeros" name="precioc" placeholder="(opcional)" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                                </div>

                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-fw fa-dollar"></i> Venta</span>
                                    </div>
                                    <input required placeholder="0.00" type="text" class="form-control numeros" name="preciov" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                                </div> 
                            </div>

                            <div class="d-flex flex-wrap flex-xl-nowrap flex-md-nowrap">
                                <div class="d-flex flex-nowrap input-group input-group-sm mb-3 mr-5 w-50 " >
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Stock</span>
                                    </div>
                                    <input  id="stock" disabled  type="text" class="form-control cantidad text-center" name="stock" >
                                </div>
                                <table class="text text-nowrap">
                                <tr><td><input value="1" type="checkbox" name="enable" id="check"></td><td> Incluir stock</td></tr>
                            </table>
                        </div>
                            </div>
                        <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button type="submit" id="guardaInv" class="btn btn-primary">Guardar</button>
                              </div>
                    </form>
                    </div>
                    <div class="tab-pane fade" id="nav-servicio" role="tabpanel" aria-labelledby="nav-servicio-tab">

                        {{-- <div class="d-flex flex-nowrap my-3 ml-4">
                            <form>
                                <input value="folio" checked type="radio" name="enable" id="efolio"> Folio
                                <input class="ml-4"  value="nombre" type="radio" name="enable" id="enombre"> Consultar por nombre de cliente
                            </form>
                        </div> --}}


                                <div id="inputCliente" class="input-group input-group-sm mb-3 mt-4">

                                    <select  name="bcliente" id="bCliente" >
                                        <option></option>
                                        @foreach ($cliente as $item)
                                            <option value="{{$item->id}}">{{$item->nombre.' '.$item->apellido}}</option>    
                                        @endforeach
                                    </select>
                                   
                                </div>
                            <div class="d-xl-flex d-md-flex ">
                                <div id="inputFolio" class="input-group input-group-sm mb-3 w-xl-50 w-md-50 ">
                                        
                                    <input id="idFolio" placeholder="Folio" type="text" class="form-control numeros" name="folio" placeholder="(opcional)" >

                                    <button id="buscaFolio" class="btn btn-outline-secondary" type="button"><i class="fa fa-fw fa-search"></i></button>
                                </div>

                                <div class="input-group input-group-sm mb-3 inp-num ml-xl-3 ml-md-3 d-flex flex-nowrap">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" ><i class="fa fa-fw fa-dollar"></i></span>
                                        </div>
                                        <input id="servPrecio" autocomplete="off" placeholder="0.00" type="text" class="form-control numeros" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                                </div>
                                <button id="agregaServ" class="btn btn-primary ml-xl-3 ml-md-3" type="button">Agregar</button>
                            </div>

                                <div id="response" class="mt-4">
                                    <table class="w-100" id="resultado">

                                    </table>
                                    <p id="descrip" class="mt-4"></p>
                                </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
      </div>

        <!-- Modal cobrar-->
<div class="modal fade" id="cobrarModal" tabindex="-1" role="dialog" aria-labelledby="cobrarModal" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" >Cobrar</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body d-flex flex-column">

                    <h2 class="mx-auto"><span id="modTotal" class="badge badge-danger">Total<i class="fa fa-fw fa-dollar"></i>0.00</span></h2>
                
                <div class="input-group d-flex flex-nowrap mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Efectivo</span>
                    </div>
                    <input  id="recibo" type="text" value="" placeholder="0.00" class="form-control numeros" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                </div>

                <div class="input-group d-flex flex-nowrap ">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Cambio</span>
                    </div>
                    <input disabled style="font-weight:bold" id="cambio" type="text" value="" placeholder="0.00" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                </div>
            </div>
            <div class="modal-footer">
            <button  id="vender" role="button" class="btn btn-primary btn-lg btn-block "><i class="fa fa-fw  fa-file-text-o"></i> Generar recibo</button>
            </div>
          </div>
        </div>
      </div>

@endsection