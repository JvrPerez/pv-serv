<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nota de venta</title>
   
    <style>

        @page{margin-top: 9mm;}
        .margen{
            width: 190mm;
            height: 125mm;
            position: relative;
            /* border: 1px solid red; */
        }

        img{
            height: 20mm;
            margin-left: 10px;
            margin-top: 12px;
        }

        #datos{
            position: fixed;
            left: 200px;
            top: 10px;
            font-size: 12px;
            margin-top:15px;
        }

        #folio{
            position: absolute;
            top:-12px;
            left: 550px;
        }
        #fo
        {
            font-size: 15px;
        }

        #fe{
            font-size: 12px;
        }

        #fo-num{            
            color:darkred;
        }

        .cliente{
            width: 100%;
            margin-top:10px;
            font-size: 12px;
            border: 1px solid #252525;
            margin-bottom: 10px;
            border-radius: 5px;
        }
        .detventa{
            width: inherit;
            border-collapse: collapse;
        }
        .detventa tbody td {
            border-collapse: collapse;
            padding-top: 5px;
        }
        .divventa{
            width: 100%;
            margin-top: 10px;
        }

        .detventa thead{
            border-bottom: 1px solid #252525;
            text-align: center;
        }

        .detventa tbody{
            text-align: center;
            font-size: 12px;
        }

        .detalles{
            margin-left: 10px;
            font-size: 12px;
        }

        .divt{
            font-weight: bold;
            text-align:right;
        }

        .foot{
            width:190mm;
            position:fixed;
            top: 435px;
            text-align: justify;
            font-size: 10px;
            padding: 5px;
        }

        .letra{
            float: left;
            text-align: center;
            border: 1px solid #252525;
            padding: 5px;
            border-radius: 5px;
            font-size: 11px;
            width: 260px;
        }

        .obv{
            width: 320px;
            float: right;
            margin-top: 10px;
            padding-right: 10px;
            padding-left: 10px;
            font-size: 12px;
            border: 1px solid #252525;
            border-radius: 5px;

        }
        .desc{
            width: 320px;
            padding-right: 10px;
            padding-left: 10px;
            font-size: 12px;
            border: 1px solid #252525;
            border-radius: 5px;
        }

        .det{
            width: 80%;
            font-size: 12px;
            border: 1px solid #252525;
            border-radius: 5px;
        }
    </style>
</head>
<body>
    <div class="margen">

        <div class="header">
                <img src="{{public_path().'/img/logotinta.jpg'}}" alt="">
                <div id="folio">
                    <p>NOTA DE SERVICIO</p>
                <p id="fo">Folio: <span id="fo-num">{{str_pad($servicio->id, 6, '0', STR_PAD_LEFT)}}</span>
                    <br><span id="fe">Fecha: {{$servicio->created_at}}</span>
                </p>
            
                        <p id="datos">3a Norte #724-A entre 6a Y 7a Poniente 
                            <br>Centro Tuxtla Gutierrez, Chiapas. 
                            <br>Correo: mastintatoner@hotmail.com
                            <br>Tel. 961 18 599 59
                        </p>
                </div>
        </div>

        <table class="cliente">
            <tr><td>Nombre: {{$servicio->cliente->nombre}}</td></tr>
            <tr><td>Direccion: {{$servicio->cliente->direccion}}</td></tr>
            <tr><td>Ciudad: {{$servicio->cliente->ciudad}}</td></tr>
            <tr><td>Telefono: {{$servicio->cliente->telefono}}</td></tr>
        </table>

        <div class="det divventa">
            <table class="detalles">
                <tr><td>Tipo de servicio: {{$servicio->tipo}}</td></tr>
                <tr><td>Marca: {{$servicio->marca}}</td></tr>
                <tr><td>Modelo: {{$servicio->modelo}}</td><td>N/S: {{$servicio->serial}}</td></tr>
            </table>
        </div>

        <div class="obv">
                <p>Observaciones:</p>
                <p>@php echo nl2br($servicio->observaciones) @endphp</p>
        </div>
  
        <div class="desc">
            <p>Descripción:</p>
            <p>@php echo nl2br($servicio->descripcion) @endphp</p>
        </div>


    <div class="foot">*Garantía de 30 días presentando su nota. 
        <br>*El cliente es consciente que en las impresoras de tinta puede taparse el cabezal de impresión si no se usa constantemente.
        <br>*Al notificar que el equipo esta listo, si no se recoge después de 15 días no nos haremos resposables del mismo. 
    </div>
</div>
    

</body>
</html>