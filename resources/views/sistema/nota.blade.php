<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nota de venta</title>
   
    <style>
        @page{margin: 10mm;}

        .margen{
            width: 120mm;
            height: 190mm;
            position: relative;
            margin-left:auto;
        }
        img{
            width: 50mm;
            margin-top: 18px;
        }
        #datos{
            font-size: 10px;
            margin-top:15px;
        }
        #folio{
            position: absolute;
            top:-12px;

            margin-left: 60px;
        }
        #fo
        {
            font-size: 15px;
        }

        #fe{
            font-size: 12px;
        }

        #fo-num{            
            color:darkred;
        }

        .cliente{
            width: 100%;
            margin-top:10px;
            font-size: 13px;
            border: 1px solid #252525;
            border-radius: 5px;
        }
        .detventa{
            width: inherit;
            border-collapse: collapse;
        }
        .detventa tbody td {
            border-collapse: collapse;
            padding-top: 5px;
        }
        .divventa{
            width: 100%;
            height: 330px;
            margin-top: 15px;
        }

        .detventa thead{
            border-bottom: 1px solid #252525;
            text-align: center;
        }

        .detventa tbody{
            text-align: center;
            font-size: 12px;
        }

        .totales{
            padding: 5px;
            border: 1px solid #252525;
            border-radius: 5px;
            width: 160px;
            float: right;
            font-size: 15px;
            

        }

        .divt{
            font-weight: bold;
            text-align:right;
        }

        .foot{
            width:110mm;
            position:absolute;
            bottom: 10px;
            text-align: justify;
            font-size: 12px;
            padding: 5px;
        }

        .letra{
            float: left;
            text-align: center;
            border: 1px solid #252525;
            padding: 5px;
            border-radius: 5px;
            font-size: 11px;
            width: 260px;
        }
    </style>
</head>
<body>
    <div class="margen">

        <div class="header">
                <img src="{{public_path().'/img/logotinta.jpg'}}" alt="">
                <div id="folio">
                    <p>NOTA DE VENTA</p>
                <p id="fo">Folio: <span id="fo-num">{{str_pad($venta->id, 6, '0', STR_PAD_LEFT)}}</span>
                    <br><span id="fe">Fecha: {{$venta->created_at}}</span>
                </p>
            
                        <p id="datos">3a Norte #724-A entre 6a Y 7a Poniente 
                            <br>Col. Centro Tuxtla Gutierrez, Chiapas. 
                            <br>Correo: mastintatoner@hotmail.com
                            <br>Tel. 961 18 599 59
                        </p>
                </div>
        </div>

        <table class="cliente">
            <tr><td>Nombre: {{$venta->cliente->nombre}}</td></tr>
            <tr><td>Direccion: {{$venta->cliente->direccion}}</td></tr>
            <tr><td>Ciudad: {{$venta->cliente->ciudad}}</td></tr>
            <tr><td>Telefono: {{$venta->cliente->telefono}}</td></tr>
        </table>

        <div class="cliente divventa">
        <table class=" detventa">
            <thead >
                <tr>
                    {{-- <th>#</th> --}}
                    <th>Descripción</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                    <th>Importe</th>
                </tr>
            </thead>
            <tbody >
                @foreach ($venta->inventarios as $item)
                    <tr>
                    {{-- <td>{{$item->id}}</td> --}}
                    <td  style="text-align:left;padding-left:10px;">{{$item->descripcion}}</td>
                    <td>{{$item->pivot->cantidad}}</td>
                    <td>{{number_format($item->preciov, 2)}}</td>
                    <td>{{number_format($item->pivot->importe,2)}}</td>
                    </tr>
                @endforeach

                @foreach ($venta->servicios as $item)
                <tr>
                {{-- <td>{{$item->id}}</td> --}}
                <td  style="text-align:left;padding-left:10px;">{{$item->tipo}}</td>
                <td>{{$item->pivot->cantidad}}</td>
                <td>{{number_format($item->pivot->importe, 2)}}</td>
                <td>{{number_format($item->pivot->importe,2)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
  

    <div>
        <p class="letra">{{$letras->convertir($venta->total,'pesos','centavos')}}</p>

        <div class="totales">
                <table>
                    <tr><td class="divt">Subtotal $</td><td>{{number_format($venta->subtotal,2)}}</td></tr>
                    <tr><td class="divt">Descuento $</td><td>{{number_format($venta->descuento,2)}}</td></tr>
                    <tr><td class="divt">Total $</td><td>{{number_format($venta->total,2)}}</td></tr>
                       
                </table>
        </div>
    </div>

    <div class="foot">*Es necesario presentar esta nota para hacer validada la garantía por cualquier detalle o defecto en su articulo.</div>
</div>
    

</body>
</html>