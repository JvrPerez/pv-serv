@extends('layouts.sistema')

@section('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
{{-- <script src="{{asset('vendor/datatables/buttons.colVis.min.js')}}"></script> --}}
<script src="{{asset('vendor/datatables/dataTables.select.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('js/diarioServicio.js')}}"></script>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('css/venta.css')}}">
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('vendor/datatables/responsive.dataTables.min.css')}}">
<link rel="stylesheet" href="{{asset('vendor/datatables/select.dataTables.min.css')}}">
<link href="{{asset('vendor/datatables/buttons.dataTables.min.css')}}" rel="stylesheet">
@endsection

@section('contenido')
<nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li style="font-size:20px;"  class="breadcrumb-item active" aria-current="page"><i class="fa fa-fw  fa-wrench" ></i>Servicios del día</li>
    
          <button id="btnTerminado" class=" p-1 ml-auto btn btn-outline-success" title="Marcar como terminado" ><i style="font-size:23px;" class="fa fa-fw fa-check-circle" ></i></button>
          <button id="btncomprobante" class="ml-2 p-1 btn btn-outline-primary" title="Ver comprobante" ><i style="font-size:23px;" class="fa fa-fw fa-search" ></i></button>
          <button id="btndeleteservicio" class="ml-2 px-1 btn btn-outline-danger" title="Eliminar registro"><i style="font-size:23px;" class="fa fa-fw fa-trash" ></i></button>
        </ol>
</nav>


<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-edit"></i> Formulario para búsqueda de servicios por fecha</div>
    <form id="form-consulta" action="{{route('servicio.fconsulta')}}" method="post">
        @csrf
        <div class="card-body" >

      <div class="d-flex nowrap w-75">
        <div class="">Fecha de inicio para la búsqueda</div>

        <div class="input-group input-group-sm mb-3 ml-auto w-50"> 
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-fw fa-calendar"></i></span>
            </div>
            <input type="date" name="inicio" required class="form-control " aria-label="Small" aria-describedby="inputGroup-sizing-sm">
          </div>
        </div>

        <div class="d-flex nowrap w-75">
            <div class="">Fecha final para la búsqueda</div>
    
            <div class="input-group input-group-sm mb-3 ml-auto w-50 "> 
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-fw fa-calendar"></i></span>
                </div>
                <input type="date" name="final" required class="form-control " aria-label="Small" aria-describedby="inputGroup-sizing-sm">
              </div>
            </div>
    </div>

    <div class="card-footer d-flex">
      <button class="btn btn-sm btn-primary ml-auto" type="submit"><i class="fa fa-fw fa-search"></i> Realizar búsqueda</button>
    </div>
  </form>

</div>
<div id="tableCont">
@if (isset($servicio))
    <div class="card p-3 mb-3">
        
            <div class="" id="tableCont">
         
                    <table id="tableClientes" class=" table table-bordered display responsive nowrap"  style="font-size:15px;" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                            <th data-priority="1" >Folio</th>
                            <th data-priority="2">Cliente</th>
                            <th data-priority="3">Tipo</th>
                            <th data-priority="4">Marca</th>
                            <th data-priority="5">Modelo</th>
                            <th data-priority="6">Recibido</th>
                            <th data-priority="7">Terminado</th>
                            <th>Descripción</th>
                            <th>Observaciones</th>
            
                        </tr>
                      </thead>
            
                      <tbody>
                          @foreach ($servicio as $item)
                          @php
            $entregado='No';
                if($item->terminado!=null)
                {
                    $entregado = $item->terminado;
                }
            
            @endphp
                          <tr>
                            <td>{{str_pad($item->id, 6, "0", STR_PAD_LEFT)}}</td>
                            <td>{{$item->cliente->nombre}}</td>
                            <td>{{$item->tipo}} </td>
                            <td>{{$item->marca}}</td>
                            <td>{{$item->modelo}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>{{$entregado}}</td>
                            <td>@php echo nl2br($item->descripcion) @endphp</td>
                            <td>@php echo nl2br($item->observaciones) @endphp</td>
            
                            
                        </tr>
                          @endforeach
                        
                    </tbody>
                </table>
              </div>

    </div>
@endif

</div>


{{-- ------AVISO------ --}}
<div class="modal fade" id="deleteClienteModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Eliminar</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p id="modalBody"></p>
                </div>
                <div class="modal-footer">
                  <button id="delCliente" type="button" class="btn btn-primary">Si</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                </div>
          </div>
        </div>
      </div>
  
  
      
  {{-- ------AVISO TERMINADO------ --}}
    <div class="modal fade" id="termServModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Marcar como terminado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p id="modalBodyServ"></p>
                  </div>
                  <div class="modal-footer">
                    <button id="termServ" type="button" class="btn btn-primary">Si</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                  </div>
            </div>
          </div>
        </div>
  @endsection