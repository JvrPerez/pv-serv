@extends('layouts.sistema')

@section('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
{{-- <script src="{{asset('vendor/datatables/buttons.colVis.min.js')}}"></script> --}}
<script src="{{asset('vendor/datatables/dataTables.select.min.js')}}"></script>
{{-- <script src="{{asset('vendor/datatables/dataTables.responsive.min.js')}}"></script> --}}
<script src="{{asset('js/diarioVenta.js')}}"></script>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('css/venta.css')}}">
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
{{-- <link rel="stylesheet" href="{{asset('vendor/datatables/responsive.dataTables.min.css')}}"> --}}
<link rel="stylesheet" href="{{asset('vendor/datatables/select.dataTables.min.css')}}">
<link href="{{asset('vendor/datatables/buttons.dataTables.min.css')}}" rel="stylesheet">
@endsection

@section('contenido')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li style="font-size:20px;"  class="breadcrumb-item active" aria-current="page"><i class="fa fa-fw fa-cart-arrow-down" ></i> Consulta general de ventas</li>
      <button title="Ver nota de venta" id="btnview" class="ml-auto p-1 btn btn-outline-success" ><i style="font-size:23px;" class="fa fa-fw fa-search" ></i></button>
      <button title="Eliminar registro" id="btndelete" class="ml-2 px-1 btn btn-outline-danger" ><i style="font-size:23px;" class="fa fa-fw fa-trash" ></i></button>
      {{-- <button class=" p-1 ml-auto btn btn-sm btn-outline-success"  data-toggle="modal" data-target="#addClienteModal"><i style="font-size:32px;" class="fa fa-fw fa-plus-circle" ></i></button> --}}

    </ol>
</nav>


<div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table"></i> Tabla de ventas</div>
    <div class="card-body" >

      <div class="table-responsive" id="tableCont">
         
        <table id="tableClientes" class=" table table-bordered nowrap"  style="font-size:15px;" width="100%" cellspacing="0">
          <thead>
            <tr>
                <th >Folio</th>
                <th >Cliente</th>
                <th>Productos</th>
                <th>Servicios</th>
                <th >Subtotal</th>
                <th >Descuento</th>
                <th >Total</th>
                <th>Fecha</th>
            </tr>
          </thead>

          <tbody>
              @foreach ($ventas as $item)
              @php
$cant=0;
$serv=0;
    foreach ($item->inventarios as $inv)
    {
       $cant = $cant + $inv->pivot->cantidad;
    }
    foreach ($item->servicios as $inv)
    {
       $serv = $serv + $inv->pivot->cantidad;
    }
@endphp
              <tr>
                <td>{{str_pad($item->id, 6, "0", STR_PAD_LEFT)}}</td>
                <td>{{$item->cliente->nombre}}</td>
                <td class="text-center">{{$cant}} </td>
                <td class="text-center">{{$serv}}</td>
                <td>$ {{number_format($item->subtotal,2)}}</td>
                <td>$ {{number_format($item->descuento,2)}}</td>

                <td>$ {{number_format($item->total,2)}}</td>
                <td>{{$item->created_at}}</td>
                
            </tr>
              @endforeach
            
        </tbody>
    </table>
  </div>
</div>

</div>


{{-- ------AVISO------ --}}
<div class="modal fade" id="deleteClienteModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Eliminar</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p id="modalBody"></p>
                </div>
                <div class="modal-footer">
                  <button id="delCliente" type="button" class="btn btn-primary">Si</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                </div>
          </div>
        </div>
      </div>
  @endsection