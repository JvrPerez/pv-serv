@extends('layouts.sistema')

@section('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
{{-- <script src="{{asset('vendor/datatables/buttons.colVis.min.js')}}"></script> --}}
<script src="{{asset('vendor/datatables/dataTables.select.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('js/cliente.js')}}"></script>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('css/venta.css')}}">
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('vendor/datatables/responsive.dataTables.min.css')}}">
<link rel="stylesheet" href="{{asset('vendor/datatables/select.dataTables.min.css')}}">
<link href="{{asset('vendor/datatables/buttons.dataTables.min.css')}}" rel="stylesheet">
@endsection

@section('contenido')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li style="font-size:20px;"  class="breadcrumb-item active" aria-current="page"><i class="fa fa-fw fa-address-book icon" ></i> Consultar clientes</li>
      <b id="id"></b>
      <button title="Agregar cliente" class=" p-1 ml-auto btn btn-sm btn-outline-success"  data-toggle="modal" data-target="#addClienteModal"><i style="font-size:32px;" class="fa fa-fw fa-plus-circle" ></i></button>
      <button title="Modificar un registro" id="btnedit" class="p-1 ml-2 btn btn-sm btn-outline-warning" ><i style="font-size:23px;" class="fa fa-fw fa-edit" ></i></button>
    <button title="Eliminar registro" id="btndelete" class="p-1 ml-2 btn btn-sm btn-outline-danger" ><i style="font-size:23px;" class="fa fa-fw fa-trash-o " ></i></button>
    </ol>
</nav>

<div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table"></i> Tabla de clientes</div>
    <div class="card-body" >

      <div class="table-responsive" id="tableCont">
         
        <table id="tableClientes" class=" table table-bordered responsive nowrap "  width="100%" cellspacing="0">
          <thead>
            <tr>
                <th data-priority="1">ID</th>
                <th data-priority="2">Nombre</th>
                <th data-priority="3">Telefono</th>
                <th data-priority="4">Correo</th>
                <th data-priority="5">Registrado</th>
                <th data-priority="6">#Compras</th>
                <th data-priority="7">$Compras</th>
                <th>#Servicios</th>
                <th>R.F.C</th>
                <th>Dirección</th>
                <th>Ciudad</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Telefono</th>
                <th>Correo</th>
                <th>Registrado</th>
                <th>#Compras</th>
                <th>$Compras</th>
                <th>#Servicios</th>
                <th>R.F.C</th>
                <th>Dirección</th>
                <th>Ciudad</th>
            </tr>
          </tfoot>
          <tbody>
              @foreach ($cliente as $item)
              <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->nombre}}</td>
                <td>{{$item->telefono}}</td>
                <td>{{$item->correo}}</td>
                <td>{{$item->created_at->format('d-m-Y')}}</td>
                <td>{{$item->ventas->count()}}</td>
                <td>$ {{number_format($item->ventas->sum('total'),2)}}</td>
                <td>{{$item->servicios->count()}}</td>
                <td>{{$item->rfc}}</td>
                <td>{{$item->direccion}}</td>
                <td>{{$item->ciudad}}</td>
                  </tr>
              @endforeach
            
        </tbody>
    </table>
  </div>
</div>

</div>



<!-- Modal  para nuevo cliente -->
<div class="modal fade" id="addClienteModal" tabindex="-1" role="dialog" aria-labelledby="addClienteModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addClienteModal">Nuevo cliente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('Cliente.store')}}" id="addCliente" method="post">
                    @csrf
                <input class="form-control mb-3" type="text" name="nombre" placeholder="Cliente/Empresa*" required id="">
                <div class="form-group d-flex">
                    <input class="form-control mr-3" placeholder="RFC (opcional)" type="text"  pattern="(^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$)" name="rfc" id="">
                    <input class="form-control" placeholder="Correo (opcional)" type="email" name="correo" id="">
                </div> 
                <input class="form-control mb-3" placeholder="Direccion (opcional)" type="text" name="direccion" id="">
                <div class="form-group d-flex">
                    <input class="form-control mr-3" placeholder="Telefono (opcional)" type="text" pattern="(^([\d]{7}|[\d]{10})$)" name="telefono" id="">
                    <input class="form-control" placeholder="Ciudad (opcional)" type="text" name="ciudad" id="">
                </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button id="addCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" form="addCliente" class="btn btn-primary">Guardar</button>
        </div>
    
      </div>
    </div>
  </div>


  <!-- Modal  para modificar cliente -->
<div class="modal fade" id="editClienteModal" tabindex="-1" role="dialog" aria-labelledby="editClienteModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="edClienteModal">Editar cliente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('cliente.editar')}}" id="editCliente" method="POST">
                @csrf
            <div class="input-group input-group-sm mb-3 w-25 ">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">ID</span>
                </div>
                <input id="idcliente" class="form-control text-center" readonly type="text" name="id">
            </div>
         

                <input class="form-control mb-3" type="text" name="nombre" placeholder="Cliente/Empresa*" required id="cnombre">
                <div class="form-group d-flex">
                    <input class="form-control mr-3" placeholder="RFC (opcional)" type="text"  pattern="(^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$)" name="rfc" id="crfc">
                    <input class="form-control" placeholder="Correo (opcional)" type="email" name="correo" id="ccorreo">
                </div> 
                <input class="form-control mb-3" placeholder="Direccion (opcional)" type="text" name="direccion" id="cdireccion">
                <div class="form-group d-flex">
                    <input class="form-control mr-3" placeholder="Telefono (opcional)" type="text" pattern="(^([\d]{7}|[\d]{10})$)" name="telefono" id="ctelefono">
                    <input class="form-control" placeholder="Ciudad (opcional)" type="text" name="ciudad" id="cciudad">
                </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button id="addCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" form="editCliente" class="btn btn-primary">Guardar</button>
        </div>
    
      </div>
    </div>
  </div>

{{-- ------AVISO------ --}}
  <div class="modal fade" id="deleteClienteModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Eliminar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p id="modalBody"></p>
              </div>
              <div class="modal-footer">
                <button id="delCliente" type="button" class="btn btn-primary">Si</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              </div>
        </div>
      </div>
    </div>
@endsection
