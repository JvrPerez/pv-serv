@extends('layouts.sistema')

@section('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
{{-- <script src="{{asset('vendor/datatables/buttons.colVis.min.js')}}"></script> --}}
<script src="{{asset('vendor/datatables/dataTables.select.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('js/inventario.js')}}"></script>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('css/venta.css')}}">
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('vendor/datatables/responsive.dataTables.min.css')}}">
<link rel="stylesheet" href="{{asset('vendor/datatables/select.dataTables.min.css')}}">
<link href="{{asset('vendor/datatables/buttons.dataTables.min.css')}}" rel="stylesheet">
@endsection

@section('contenido')


<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Nuevo productos</a>
      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Nueva categorias</a>
    </div>
  </nav>
  <div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li style="font-size:20px;"  class="breadcrumb-item active" aria-current="page"><i class="fa fa-fw fa-cubes" ></i> Inventario</li>
              <b id="id"></b>
              <button title="Modificar un registro" id="btnedit" class="p-1 ml-auto btn btn-sm btn-outline-warning" ><i style="font-size:23px;" class="fa fa-fw fa-edit" ></i></button>
            <button title="Eliminar registro" id="btndelete" class="p-1 ml-2 btn btn-sm btn-outline-danger" ><i style="font-size:23px;" class="fa fa-fw fa-trash-o " ></i></button>
            </ol>
        </nav>
        
<div class="card my-4 mx-4">
    <div class="card-header">Nuevo producto</div>
        <form class="mx-5" action="{{route('inventario.store')}}" method="POST" id="InveForm">
            <div id="loadCategoria" class="mt-4"> 

              <select  name="categoria_id" id="categoria">
                <option></option>
                @foreach ($categoria as $item)
              <option value="{{$item->id}}">{{$item->nombre}}</option>
                @endforeach

              </select>
                <div class="input-group input-group-sm mb-3 mt-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroup-sizing-sm">Descripción</span>
                    </div>
                    <input id="descripcion" required type="text" class="form-control" name="descripcion" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                </div>
              </div>

                <div class="d-flex ">
                    <div class="input-group input-group-sm mb-3 mr-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-fw fa-dollar"></i> Compra</span>
                        </div>
                        <input id="precioc" type="text" class="form-control numeros" name="precioc" placeholder="(opcional)" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-fw fa-dollar"></i> Venta</span>
                        </div>
                        <input id="preciov" required placeholder="0.00" type="text" class="form-control numeros" name="preciov" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                    </div> 
                </div>

                <div class="d-flex ">
                    <div class="input-group input-group-sm mb-3 mr-5 " style="width:20%;">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Stock</span>
                        </div>
                        <input  id="stock" disabled  type="text" class="form-control cantidad text-center numeros" name="stock" >
                    </div>
                    <table>
                    <tr><td><input value="1" type="checkbox" name="enable" id="check"></td><td> Incluir stock</td></tr>
                </table>
            </div>
                    <div class="ml-auto my-3 w-100 d-flex">
                    <button onclick="$('#InveForm')[0].reset();" type="button" class="btn btn-secondary  ml-auto mx-3" data-dismiss="modal">Cancelar</button>
                    <button type="submit"  class="btn btn-primary">Guardar</button>
                  </div>
                </div>
                  
      </form>

<div class="card p-3" id="invelist">
      <div class="table table-responsive">
        <table style="font-size:15px;" class="table table-bordered display responsive nowrap" id="tableInventario" width="100%" cellspacing="0">

          <thead>
            <tr>
              <th>ID</th>
              <th>Descripción</th>
              <th>Categoria</th>
              <th>Stock</th>
              <th>Precio compra</th>
              <th>Precio venta</th>
              <th>Disminuir</th>
              <th>Registrado</th>
              <th>Actualizado</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($inventario as $item)
                
            @php
              $enable='';
              if ($item->enable) {
                $enable='Si';
              } else {
                $enable = 'No';
              }
            
            @endphp
              <tr>
              <td>{{$item->id}}</td>
              <td>{{$item->descripcion}}</td>
              <td>{{$item->categoria['nombre']}}</td>
              <td>{{$item->stock}}</td>
              <td>{{number_format($item->precioc,2)}}</td>
              <td>{{number_format($item->preciov,2)}}</td>
              <td>{{$enable}}</td>
              <td>{{$item->created_at}}</td>
              <td>{{$item->updated_at}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    </div>

    {{-- ---------------NUEVO TAB---------------------- --}}
    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li style="font-size:20px;"  class="breadcrumb-item active" aria-current="page"><i class="fa fa-fw fa-cube" ></i> Categorias</li>
              <b id="id"></b>
              <button title="Modificar un registro" id="btneditCat" class="p-1 ml-auto btn btn-sm btn-outline-warning" ><i style="font-size:23px;" class="fa fa-fw fa-edit" ></i></button>
            <button title="Eliminar registro" id="btndeleteCat" class="p-1 ml-2 btn btn-sm btn-outline-danger" ><i style="font-size:23px;" class="fa fa-fw fa-trash-o " ></i></button>
            </ol>
        </nav>
      
      <div class="card m-5 p-5">
        <form id="CatForm" action="{{route('categoria.store')}}" method="post">
            <div class="input-group input-group-sm d-flex flex-nowrap mr-4">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Categoria</span>
                </div>
                <input  class="form-control" type="text" name="nombre">
                <button type="submit" class="btn btn-primary ml-3">Guardar</button>
            </div>

        </form>
      </div>

      <div class="card p-4" id="divCat">
        <table class="table table-bordered " id="tableCat" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
            </tr>
          </thead>
          <tbody>
@foreach ($categoria as $item)
    <tr>
    <td>{{$item->id}}</td>
    <td>{{$item->nombre}}</td>
    </tr>
@endforeach
          </tbody>
        </table>
      </div>


    </div>
  </div>




{{-- ------AVISO--EDITA INVENTARIO---- --}}
<div class="modal fade" id="editInvModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Editar registro</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <form class="" action="{{route('inventario.edit')}}" method="POST" id="InveFormEdit">
              <div class="input-group input-group-sm w-25">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">ID</span>
                </div>
                <input type="text" id="inve_id" class="form-control" readonly name="id">
            </div>
              
              
              <div class="mt-4"> 
                <select  name="categoria_id" id="categoria2">
                  <option></option>
                  @foreach ($categoria as $item)
                <option value="{{$item->id}}">{{$item->nombre}}</option>
                  @endforeach
  
                </select>
                  <div class="input-group input-group-sm mb-3 mt-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Descripción</span>
                      </div>
                      <input id="descripcion2" required type="text" class="form-control" name="descripcion" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                  </div>
                </div>
  
                  <div class="d-flex ">
                      <div class="input-group input-group-sm mb-3 mr-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-fw fa-dollar"></i> Compra</span>
                          </div>
                          <input id="precioc2" type="text" class="form-control numeros" name="precioc" placeholder="(opcional)" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                      </div>
  
                      <div class="input-group input-group-sm mb-3">
                          <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-fw fa-dollar"></i> Venta</span>
                          </div>
                          <input id="preciov2" required placeholder="0.00" type="text" class="form-control numeros" name="preciov" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                      </div> 
                  </div>
  
                  <div class="d-flex ">
                      <div class="input-group input-group-sm mb-3 mr-5 " style="width:40%;">
                          <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroup-sizing-sm">Stock</span>
                          </div>
                          <input  id="stock2" disabled  type="text" class="form-control cantidad text-center numeros" name="stock" >
                      </div>
                      <table>
                      <tr><td><input value="1" type="checkbox" name="enable" id="check2"></td><td> Incluir stock</td></tr>
                  </table>
              </div>
                  
            </form>



          </div>
          <div class="modal-footer">
            <button form="InveFormEdit" onclick="$('#InveFormEdit')[0].reset();" type="button" class="btn btn-secondary  ml-auto mx-3" data-dismiss="modal">Cancelar</button>
            <button form="InveFormEdit" type="submit"  class="btn btn-primary">Guardar</button>
          </div>
    </div>
  </div>
</div>



{{-- ------AVISO--EDITA CATEGORIA---- --}}
<div class="modal fade" id="editCatModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title">Editar categoria</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              
                <form id="CatFormEdit" action="{{route('categoria.edit')}}" method="post">

                    <div class="input-group input-group-sm d-flex flex-nowrap w-50 mr-5 mb-3 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Categoria</span>
                        </div>
                        <input id="id_cat" class="form-control" type="text" readonly name="id">
                    </div>

                    <div class="input-group input-group-sm d-flex flex-nowrap mr-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Categoria</span>
                        </div>
                        <input id="nombre" class="form-control" type="text" name="nombre">
                        <button type="submit" class="btn btn-primary ml-3">Guardar</button>
                    </div>
        
                </form>


            </div>

      </div>
    </div>
  </div>
     



{{-- ------AVISO-DELETE CLIENTE----- --}}
  <div class="modal fade" id="deleteClienteModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Eliminar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p id="modalBody"></p>
              </div>
              <div class="modal-footer">
                <button id="delInventario" type="button" class="btn btn-primary">Si</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              </div>
        </div>
      </div>
    </div>


    {{-- ------AVISO--DELETE CATEGORIA---- --}}
  <div class="modal fade" id="deleteCatModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Eliminar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p id="modalBodyCat"></p>
              </div>
              <div class="modal-footer">
                <button id="delCat" type="button" class="btn btn-primary">Si</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              </div>
        </div>
      </div>
    </div>
@endsection
