@extends('layouts.sistema')

@section('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
{{-- <script src="{{asset('vendor/datatables/buttons.colVis.min.js')}}"></script> --}}
<script src="{{asset('vendor/datatables/dataTables.select.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('js/cliente.js')}}"></script>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('css/venta.css')}}">
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('vendor/datatables/responsive.dataTables.min.css')}}">
<link rel="stylesheet" href="{{asset('vendor/datatables/select.dataTables.min.css')}}">
<link href="{{asset('vendor/datatables/buttons.dataTables.min.css')}}" rel="stylesheet">
@endsection

@section('contenido')
<nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li style="font-size:20px;"  class="breadcrumb-item active" aria-current="page"><i class="fa fa-fw fa-address-book icon" ></i> Consultar ventas a clientes por fecha</li>
        </ol>
    </nav>


<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-edit"></i> Formulario para búsqueda de ventas por fecha</div>
    <form id="form-consulta" action="{{route('cliente.fconsulta')}}" method="post">
        @csrf
        <div class="card-body" >

      <div class="d-flex nowrap w-75">
        <div class="">Fecha de inicio para la búsqueda</div>

        <div class="input-group input-group-sm mb-3 ml-auto w-50"> 
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-fw fa-calendar"></i></span>
            </div>
            <input type="date" name="inicio" required class="form-control " aria-label="Small" aria-describedby="inputGroup-sizing-sm">
          </div>
        </div>

        <div class="d-flex nowrap w-75">
            <div class="">Fecha final para la búsqueda</div>
    
            <div class="input-group input-group-sm mb-3 ml-auto w-50 "> 
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-fw fa-calendar"></i></span>
                </div>
                <input type="date" name="final" required class="form-control " aria-label="Small" aria-describedby="inputGroup-sizing-sm">
              </div>
            </div>
    </div>

    <div class="card-footer d-flex">
      <button class="btn btn-sm btn-primary ml-auto" type="submit"><i class="fa fa-fw fa-search"></i> Realizar búsqueda</button>
    </div>
  </form>

</div>
<div id="tableCont">
@if (isset($cliente))
  <div class="card mb-3">
  <div class="card-header">Buscando resultados de <b>{{$inicio->format('d-m-Y')}}</b> hasta <b>{{$final2->subDay()->format('d-m-Y')}}</b></div>
      <div class="card-body">
  <div class="table-responsive">
    
        <table id="tableClientes" class=" table table-bordered responsive nowrap "  style="font-size:14px;" width="100%" cellspacing="0">
          <thead>
            <tr>
                <th data-priority="1">ID</th>
                <th data-priority="2">Nombre</th>
                <th data-priority="3">Telefono</th>
                <th data-priority="4">Correo</th>
                <th data-priority="6"># Compras</th>
                <th data-priority="5">-$ Descuentos</th>
                <th data-priority="7">$ Compras</th>
                <th>Registrado</th>
                <th>R.F.C</th>
                <th>Dirección</th>
                <th>Ciudad</th>
            </tr>
          </thead>

          <tbody>
                @foreach ($cliente as $item)
                <tr>
                  <td>{{$item->id}}</td>
                  <td>{{$item->nombre}}</td>
                  <td>{{$item->telefono}}</td>
                  <td>{{$item->correo}}</td>
                  <td>{{$item->ventas}}</td>
                  <td>$ {{number_format($item->descuento)}}</td>
                  <td>$ {{number_format($item->total)}}</td>
                  <td>{{$item->created_at->format('d-m-Y')}}</td>
                  <td>{{$item->rfc}}</td>
                  <td>{{$item->direccion}}</td>
                  <td>{{$item->ciudad}}</td>
                </tr>
                @endforeach
              
          </tbody>
        </table>
      </div>
    </div>
    </div>
@endif

</div>


<!-- Modal  para nuevo cliente -->
<div class="modal fade" id="addClienteModal" tabindex="-1" role="dialog" aria-labelledby="addClienteModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="addClienteModal">Nuevo cliente</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{route('Cliente.store')}}" id="addCliente" method="post">
                        @csrf
                    <input class="form-control mb-3" type="text" name="nombre" placeholder="Cliente/Empresa*" required id="">
                    <div class="form-group d-flex">
                        <input class="form-control mr-3" placeholder="RFC (opcional)" type="text"  pattern="(^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$)" name="rfc" id="">
                        <input class="form-control" placeholder="Correo (opcional)" type="email" name="correo" id="">
                    </div> 
                    <input class="form-control mb-3" placeholder="Direccion (opcional)" type="text" name="direccion" id="">
                    <div class="form-group d-flex">
                        <input class="form-control mr-3" placeholder="Telefono (opcional)" type="text" pattern="(^([\d]{7}|[\d]{10})$)" name="telefono" id="">
                        <input class="form-control" placeholder="Ciudad (opcional)" type="text" name="ciudad" id="">
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
              <button id="addCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" form="addCliente" class="btn btn-primary">Guardar</button>
            </div>
        
          </div>
        </div>
      </div>
    
    
      <!-- Modal  para modificar cliente -->
    <div class="modal fade" id="editClienteModal" tabindex="-1" role="dialog" aria-labelledby="editClienteModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="edClienteModal">Editar cliente</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{route('cliente.editar')}}" id="editCliente" method="POST">
                    @csrf
                <div class="input-group input-group-sm mb-3 w-25 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-sm">ID</span>
                    </div>
                    <input id="idcliente" class="form-control text-center" readonly type="text" name="id">
                </div>
             
    
                    <input class="form-control mb-3" type="text" name="nombre" placeholder="Cliente/Empresa*" required id="cnombre">
                    <div class="form-group d-flex">
                        <input class="form-control mr-3" placeholder="RFC (opcional)" type="text"  pattern="(^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$)" name="rfc" id="crfc">
                        <input class="form-control" placeholder="Correo (opcional)" type="email" name="correo" id="ccorreo">
                    </div> 
                    <input class="form-control mb-3" placeholder="Direccion (opcional)" type="text" name="direccion" id="cdireccion">
                    <div class="form-group d-flex">
                        <input class="form-control mr-3" placeholder="Telefono (opcional)" type="text" pattern="(^([\d]{7}|[\d]{10})$)" name="telefono" id="ctelefono">
                        <input class="form-control" placeholder="Ciudad (opcional)" type="text" name="ciudad" id="cciudad">
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
              <button id="addCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" form="editCliente" class="btn btn-primary">Guardar</button>
            </div>
        
          </div>
        </div>
      </div>
    
    {{-- ------AVISO------ --}}
      <div class="modal fade" id="deleteClienteModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p id="modalBody"></p>
                  </div>
                  <div class="modal-footer">
                    <button id="delCliente" type="button" class="btn btn-primary">Si</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                  </div>
            </div>
          </div>
        </div>
  @endsection