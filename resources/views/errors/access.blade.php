
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pagina no encontrada</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,,300,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 36px;
                font-weight: 600;
                padding: 20px;
            }

            .subtitle{
                font-size: 30px;
                font-weight: 300;
                padding: 20px;
            }
            .redirect{
                font-size: 25px;
                font-weight: 300;
                color: blue;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title">
                    Lo sentimos, no tienes autorización para acceder a esta ruta.</div>
                    <div class="subtitle">
                        Cierra tú sesión e inicia con una cuenta autorizada.</div>

                    <a class="redirect" href="/">Volver</a>
            </div>
            
        </div>
    </body>
</html>