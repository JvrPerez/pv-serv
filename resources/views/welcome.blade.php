<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Recarga T&T</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
        

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
        <link href="{{asset('css/animate.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    </head>
    <body class="animated fadeIn">
        
        @if (Route::has('login'))
        <header>
          <div class="back">
            <img class="logo" src="{{asset('img/logo.png')}}" alt="">   
               <div class="nav-auth">
            {{-- <a href="#" onclick="mover()">Home</a>
            <a href="#" onclick="tecnico()">Tecnico</a> --}}
                    {{-- <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Registro</a> --}}
                
              </div>

            </div>
        </header>
    @endif

            <div class="content">

                <section class="slider-pages">
        
                    <article class="js-scrolling__page js-scrolling__page-1 js-scrolling--active">
                      <div class="slider-page slider-page--left">
                        <div class="slider-page--skew">
                          <div class="slider-page__content">
                          </div>
                          <!-- /.slider-page__content -->
                        </div>
                        <!-- /.slider-page--skew -->
                      </div>
                      <!-- /.slider-page slider-page--left -->
                  
                      <div class="slider-page slider-page--right">
                        <div class="slider-page--skew">
                          <div class="slider-page__content" style="background-color:#C2185B; color:#282828;">
                            <h1 class="slider-page__title animated bounceIn " style="font-size:40px;color:#FFFFFF;margin-left:30px;">
                             Para tus impresoras de laser e inyección...
                            </h1>
                            <!-- /.slider-page__title slider-page__title--big -->
                            <p style="color:#F8BBD0; margin-top:60px; font-family: 'Montserrat', sans-serif; font-size:20px;">
                                Consumibles originales de tinta o toner a precios bajos, asi como genericos y remanufacturados de calidad compatibles con las mejores marcas como 
                                <p style="color:white;font-size:20px;">HP, Brother, Samsung, Epson, Canon y Lexmark.</p> 
                            </p>
                            <div class="cont-vineta" >
                              <p><i class="fa fa-check-circle vineta"></i>Cartuchos de tinta y toner originales.</p>
                              <div style="display:flex;"><i class="fa fa-check-circle vineta"></i> <p>Cartuchos de tóner remanufacturados con excelente calidad y rendimiento.</p></div>
                            </div>

                            <!-- /.slider-page__description -->
                          </div>
                          <!-- /.slider-page__content -->
                        </div>
                        <!-- /.slider-page--skew -->
                      </div>
                      <!-- /.slider-page slider-page--right -->
                    </article>
                    <!-- /.js-scrolling__page js-scrolling__page-1 js-scrolling--active -->
                  

                  
                    <article class="js-scrolling__page js-scrolling__page-2">
                      <div class="slider-page slider-page--left">
                        <div class="slider-page--skew">
                          <div class="slider-page__content">
                            <h1 class="slider-page__title" style="font-size:50px;margin-top:60px; margin-left:20px;">
                              Impresoras...
                            </h1>
                            <!-- /.slider-page__title -->
                            <p class="slider-page__description" style="font-size:25px; margin-top:50px;">
                              Manejamos los modelos más recientes de impresoras básicas y multifuncionales de las mejores marcas para cada necesidad.<br>
                              
                            </p>

                         
                            <!-- /.slider-page__description -->
                          </div>
                          <!-- /.slider-page__content -->
                        </div>
                        <!-- /.slider-page--skew -->
                      </div>
                      <!-- /.slider-page slider-page--left -->
                  
                      <div class="slider-page slider-page--right">
                        <div class="slider-page--skew">
                          <div class="slider-page__content" >
                          <div  style="position:absolute; top:60px; filter:invert(0.9);">
                              <img src="{{asset('img/marca-brother.png')}}" alt="">
                              <img src="{{asset('img/marca-epson.png')}}" alt="">
                              <img src="{{asset('img/marca-hp.png')}}" alt="">
                            </div>
                            
                           
                          <div  style="position:absolute; bottom:30px;filter:invert(0.9);" >
                            <img src="{{asset('img/marca-lexmark.png')}}" alt="">
                            <img src="{{asset('img/marca-samsung.png')}}" alt="">
                            <img src="{{asset('img/marca-canon.png')}}" alt="">
                          </div>

                          </div>
                          <!-- /.slider-page__content -->
                        </div>
                        <!-- /.slider-page--skew -->
                      </div>
                      <!-- /.slider-page slider-page--right -->
                    </article>
                    <!-- /.js-scrolling__page js-scrolling__page-2 -->
                  
             
                  <article class="js-scrolling__page js-scrolling__page-3">
                    <div class="slider-page slider-page--left">
                      <div class="slider-page--skew">
                        <div class="slider-page__content">

                        </div>
                        <!-- /.slider-page__content -->
                      </div>
                      <!-- /.slider-page--skew -->
                    </div>
                    <!-- /.slider-page slider-page--left -->
                
                    <div id="ahorra" class="slider-page slider-page--right">
                      <div class="slider-page--skew">
                        <div class="slider-page__content" style="background-color:#1976D2;">
                            <h1 class="slider-page__title" style="font-size:50px;color:white;margin-top:50px;">
                                Ahorra más...
                              </h1>
                              <!-- /.slider-page__title -->
                              <p class="slider-page__description" style="font-size:25px; margin-top:50px;">
                                Cosigue con nosotros las mejores marcas de impresoras con sistema continuo! 
                              </p>
                              <div class="cont-vineta" >
                                <p><i class="fa fa-check-circle vineta"></i>Sin instalaciones ni mangueras.</p>
                                <div style=" display:flex; color:#252525; font-weight:400;"><p>Aprovecha la nueva generación de impresoras multifuncionales e inalámbricas, con tanques de tinta frontales, que rellenas con botellas, sin usar cartuchos.</p></div>
                              </div>
                          <!-- /.slider-page__description -->
                        </div>

                        <!-- /.slider-page__content -->
                      </div>
                      <!-- /.slider-page--skew -->
                    </div>
                    <!-- /.slider-page slider-page--right -->
                  </article>
                  <!-- /.js-scrolling__page js-scrolling__page-3 -->

                    <article id="generico" class="js-scrolling__page js-scrolling__page-4">
                        <div class="slider-page slider-page--left">
                          <div class="slider-page--skew">
                            <div class="slider-page__content" style="background-color:#b93030; padding-top:120px;">
                              <h1 class="slider-page__title" style="font-size:50px; color:white;">
                                Venta de Cartuchos Alternativos
                              </h1>
                              <!-- /.slider-page__title -->
                              <p class="slider-page__description" style="font-size:25px; margin-top:50px;">
                               Si lo prefiere contamos con Cartuchos Alternativos, que son consumibles totalmente nuevos manufacturados por fabricantes distintos al Original pero que cuentan con una excelente calidad y un costo menor al Original.
                              </p>
                              <!-- /.slider-page__description -->
                            </div>
                            <!-- /.slider-page__content -->
                          </div>
                          <!-- /.slider-page--skew -->
                        </div>
                        <!-- /.slider-page slider-page--left -->
                    
                        <div class="slider-page slider-page--right">
                          <div class="slider-page--skew">
                            <div class="slider-page__content">
  
                            </div>
                            <!-- /.slider-page__content -->
                          </div>
                          <!-- /.slider-page--skew -->
                        </div>
                        <!-- /.slider-page slider-page--right -->
                      </article>
                      <!-- /.js-scrolling__page js-scrolling__page-2 -->
                  
                  
                    <article id="tecnico" class="js-scrolling__page js-scrolling__page-5">
                      <div class="slider-page slider-page--left">
                        <div class="slider-page--skew">
                          <div class="slider-page__content">
                          </div>
                          <!-- /.slider-page__content -->
                        </div>
                        <!-- /.slider-page--skew -->
                      </div>
                      <!-- /.slider-page slider-page--left -->
                  
                      <div class="slider-page slider-page--right">
                        <div class="slider-page--skew">
                          <div class="slider-page__content"  style="padding-top:180px; background-color:#7B1FA2; color:white;">
                            <h1 class="slider-page__title">
                              Soporte y Servicio Técnico...
                            </h1>
                            <!-- /.slider-page__title -->
           
                            <p class="slider-page__description" style="font-size:25px; margin-top:50px;">
                              En Tinta & Toner contamos con asesoría técnica para el diagnóstico, reparación y mantenimiento de Impresoras Láser, de Inyección, Multifuncionales, así como para equipos de cómputo.
                            </p>
                            <!-- /.slider-page__description -->
                          </div>
                          <!-- /.slider-page__content -->
                        </div>
                        <!-- /.slider-page--skew -->
                      </div>
                      <!-- /.slider-page slider-page--right -->
                    </article>
                    <!-- /.js-scrolling__page js-scrolling__page-4 -->


                    <article class="js-scrolling__page js-scrolling__page-6">
                        <div class="slider-page slider-page--left">
                          <div class="slider-page--skew">
                            <div class="slider-page__content">
                              <h1 class="slider-page__title" style="font-size:50px; margin-top:50px;">
                               Contacto...
                              </h1>
                                <div id="contacto">
                                    <a  href="https://www.facebook.com/recarga.tintatoner.7/" ><i class="fab fa-facebook-square"></i>¡Siguenos!</a>
                                  <p><i class="fab fa-whatsapp"></i> 961 18 599 59</p>
                                  <p><i class="fas fa-phone"></i>222 34 16</p>
                                  <p><i class="fas fa-at"></i>mastintatoner@hotmail.com</p>
                                </div>

                            </div>
                            <!-- /.slider-page__content -->
                          </div>
                          <!-- /.slider-page--skew -->
                        </div>
                        <!-- /.slider-page slider-page--left -->
                    
                        <div class="slider-page slider-page--right">
                          <div class="slider-page--skew">
                            <div class="slider-page__content" >
                                <h1 class="slider-page__title" style="font-size:50px; margin-top:50px;color:white;">
                                    Encuentranos en...
                                  </h1>
                                <iframe 
                                width="590"
                                height="350"
                                frameborder="0" style="border:0;bottom:10px;position:absolute;left:130px;"
                                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBycFUm4yU14gthQUMRCivkH3tVvJDNs8Q
                                  &q=Recarga+Tinta+%26+Toner" allowfullscreen>
                              </iframe>
                            </div>
                            <!-- /.slider-page__content -->
                          </div>
                          <!-- /.slider-page--skew -->
                        </div>
                        <!-- /.slider-page slider-page--right -->
                      </article>
                      <!-- /.js-scrolling__page js-scrolling__page-2 -->
                  
                  </section>
                  <!-- /.slider-pages -->
            </div>
            <footer>
            <p>Recarga Tinta&Toner  2018 | Todos los derechos reservados | Sitio por JvrDeveloper</p>
          
            </footer>
       <script src="{{asset('js/main.js')}}"></script>
       <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    </body>
</html>
{{-- AIzaSyBycFUm4yU14gthQUMRCivkH3tVvJDNs8Q --}}