    var id_inventario=0;
    var id=0;
    var id2=0;
    var nombre2='';
    var ena;
    var descripcion='';
    var categoria='';
    var stock=0;
    var precioc=0;
    var preciov=0;
    var enable;

    var id_categoria;
    var nombre;



$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  function tableLoad(){
    var table = $('#tableInventario').DataTable({
      "lengthMenu": [[5,10, 25, 50,100], [5,10, 25, 50,100]],
      language: {
        url:'/vendor/datatables/Spanish.json',
        select: {
          rows: ""
        }
      },
      select:{style: 'single'}
    });
    
    table
    .on('select',function(e, dt, type, index){
      var rowData = table.rows(index).data().toArray();
  
      id_inventario=rowData[0][0];
      descripcion=rowData[0][1];
      categoria=rowData[0][2];
      stock=rowData[0][3];
      precioc=rowData[0][4];
      preciov=rowData[0][5];


      if(rowData[0][6]==='Si')
      {
        enable = true;
      }
      else
      {
        enable = false;
      }
    
    })
    .on('deselect',function(){
      id_inventario=0;
    });
  }
  
  tableLoad();

  $('.numeros').on('keyup',function(){
    var val= $(this).val();
    var test = /^([a-zA-zñ<>{}´+'¿?-]+)?$/
    if(test.test(String(val)))
    {
      $(this).val('');
    }
  });
   
function selectInv() {

  $("#categoria2").select2({

    language: 'es',
    placeholder:'Categoria',
    allowClear:true,
    width:'100%'
  });

  $("#categoria").select2({

    language: 'es',
    placeholder:'Categoria',
    allowClear:true,
    width:'70%'
  });

}

selectInv();


  $('#check').on('change',function(){

    if ($(this).prop('checked')) {
      $('#stock').prop('disabled',false);
    } else {
      $('#stock').prop('disabled',true);
    }
  
  });


  $('#check2').on('change',function(){

    if ($(this).prop('checked')) {
      $('#stock2').prop('disabled',false);
    } else {
      $('#stock2').prop('disabled',true);
    }
  
  });



  $('#InveForm').submit(function(e){

    e.preventDefault();
  
    var url = $(this).attr('action');
    var cont = $(this).serialize();

            $.post(url,cont,function(data){
              alert(data.result);
              
                $('#invelist').load(' #invelist',function(){
                    tableLoad();
                });
                
            },'json');
   
  });

  $('#InveFormEdit').submit(function(e){

    e.preventDefault();
  
    var url = $(this).attr('action');
    var cont = $(this).serialize();

            $.post(url,cont,function(data){
              alert(data.result);
              $('#editInvModal').modal('hide');
                $('#invelist').load(' #invelist',function(){
                    tableLoad();
                });
                
            },'json');
   
  });



  $('.cantidad').TouchSpin({
    initval: 1,
    min:1
  });



  $('#btnedit').click(function(){

    if(id_inventario!=0)
    {
      id=id_inventario;
      ena = enable;
      $('#inve_id').val(id);
      $('#descripcion2').val(descripcion);
      $('#precioc2').val(precioc);
      $('#preciov2').val(preciov);
      $('#stock2').val(stock);
      $('#editInvModal').modal('show');
      if(ena)
      {
        $('#check2').attr('checked',true);
        $('#stock2').prop('disabled',false);
      }

    }
    else
    {
      alert('Seleccione un registro de la tabla');
    }
  
  });



  $('#btndelete').click(function(){

    if(id_inventario!=0)
    {
      id=id_inventario;
      $('#modalBody').text('¿Desea eliminar el producto'+descripcion+' de su inventario? Esto afectara a las ventas registradas con este producto.');
      $('#deleteClienteModal').modal('show');
    }
    else
    {
      alert('Seleccione un registro de la tabla');
    }
  });


  $('#delInventario').click(function(){

    $.post('/sistema/inventario/delete',{id:id},function(data){
  
      
      if(data.ok)
      {
        alert(data.result)
        $('#deleteClienteModal').modal('hide');
        $('#invelist').load(' #invelist',function (){
          tableLoad();
      });
      }
      else{
        alert('No se pudo eliminar el registro.');
      }
    });
  });

//   --------------------------------------------------------------------------



  function tableLoadCat(){
    var table = $('#tableCat').DataTable({
      language: {
        url:'/vendor/datatables/Spanish.json',
        select: {
          rows: ""
        }
      },
      select:{style: 'single'}
    });
    
    table
    .on('select',function(e, dt, type, index){
      var rowData = table.rows(index).data().toArray();
  
      id_categoria=rowData[0][0],
      nombre=rowData[0][1]
    
    })
    .on('deselect',function(){
      id_categoria=0;
    });
  }

  tableLoadCat();
  


  $('#delCat').click(function(){

    $.post('/sistema/categoria/delete',{id:id2},function(data){
  
      
      if(data.ok)
      {
        alert(data.result);
        $('#deleteCatModal').modal('hide');
        $('#divCat').load(' #divCat',function (){
          tableLoadCat();
      });

      $('#loadCategoria').load(' #loadCategoria',function(){
        selectInv();
    });

    $('#invelist').load(' #invelist',function(){
      tableLoad();
  });
      }
      else{
        alert('No se pudo eliminar el registro.');
      }
    });
  });

  $('#btndeleteCat').click(function(){

    if(id_categoria!=0)
    {
      id2=id_categoria;
      $('#modalBodyCat').text('¿Desea eliminar la categoria '+nombre+' de la lista? Esto puede afectar a otros productos registrados con esta categoria.');
      $('#deleteCatModal').modal('show');
    }
    else
    {
      alert('Seleccione un registro de la tabla');
    }
  });



  $('#btneditCat').click(function(){

    if(id_categoria!=0)
    {
      id2=id_categoria;
      nombre2 = nombre;
      $('#nombre').val(nombre2);
      $('#id_cat').val(id2);
      $('#editCatModal').modal('show');
    }
    else
    {
      alert('Seleccione un registro de la tabla');
    }
  
  });


  $('#CatForm').submit(function(e){

    e.preventDefault();
  
    var url = $(this).attr('action');
    var cont = $(this).serialize();
  
            $.post(url,cont,function(data){
              alert(data.result);
              
                $('#divCat').load(' #divCat',function(){
                    tableLoadCat();
                });
                $('#loadCategoria').load(' #loadCategoria',function(){
                  selectInv();
              });
                
            },'json');
   
  });

  $('#CatFormEdit').submit(function(e){

    e.preventDefault();
  
    var url = $(this).attr('action');
    var cont = $(this).serialize();
  
            $.post(url,cont,function(data){
              alert(data.result);
              $('#editCatModal').modal('hide');
                $('#divCat').load(' #divCat',function(){
                    tableLoadCat();
                });

                $('#loadCategoria').load(' #loadCategoria',function(){
                  selectInv();
              });

              $('#invelist').load(' #invelist',function(){
                tableLoad();
            });
                
            },'json');
   
  });