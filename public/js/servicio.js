$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

function select(){
    $("#select-cliente").select2({
        language: 'es',
        placeholder:'',
        allowClear:true,
        width:'resolve'
      });
    
      $('#select-cliente').on('select2:select', function (e) {
        var cliente = e.params.data;
        idCliente=cliente.id;
        $.post('/sistema/cliente',{id:cliente.id},function (data) { 
    
              $('#detCorreo').val(data.correo);
              $('#detTelefono').val(data.telefono);
              $('#detRfc').val(data.rfc);
              $('#detDireccion').val(data.direccion);
              $('#detCiudad').val(data.ciudad);
    
              $('#detCliente').show('slow');
    
         },'json');
      });
    
      $('#select-cliente').on('select2:unselect',function(e){
        idCliente=0;
        $('#detCliente').hide('slow');
      });
    
    }

    select();




/*------------------- VENTA ----------------------*/

$('#addCliente').submit(function(e){

  e.preventDefault();

  var url = $(this).attr('action');
  var cont = $(this).serialize();


  $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });

          $.post(url,cont,function(data){
            alert(data.result);
            $('#addCancel').click();
            $('#addClienteModal').on('hidden.bs.modal', function (e) {
              $('#clientelist').load(' #clientelist',function(){

                select();
              });
              
         
            })
           
          },'json');

      
});


$('#addClienteModal').on('hidden.bs.modal', function (e) {
    $('#addCliente')[0].reset();
  });

$('#servForm').submit(function(e){

  e.preventDefault();

  var url = $(this).attr('action');
  var cont = $(this).serialize();

  $.post(url,cont,function(data){

    $('#servForm')[0].reset();
    $('#ModalConfirm').modal('hide');
    window.open('/sistema/comprobante/'+data.result);

  },'json');
});