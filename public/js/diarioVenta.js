
$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

var id_venta=0;



function tableLoad(){
  var table = $('#tableClientes').DataTable({
    "lengthMenu": [[5,10, 25, 50,100], [5,10, 25, 50,100]],
    language: {
      url:'/vendor/datatables/Spanish.json',
      select: {
        rows: ""
      }
      // buttons: {colvis: 'Cambiar columnas'}
    },

    select:{style: 'single'}
      // dom: 'Bfrtip',
      // stateSave: true,
      // buttons: [{
      //   collectionLayout: 'fixed two-column',
      //   extend: 'colvis',
      // }],

  });
  
  table
  .on('select',function(e, dt, type, index){
    var rowData = table.rows(index).data().toArray();

    id_venta=rowData[0][0],
    cliente = rowData[0][1]

  })
  .on('deselect',function(){
    id_venta=0;
  });
}




  tableLoad();


$('#btnview').click(function(){

  if(id_venta!=0)
  {
    window.open('/sistema/nota/'+id_venta);
  }
  else
  {
    alert('Seleccione un registro de la tabla');
  }


});



$('#btndelete').click(function(){

  if(id_venta!=0)
  {
    $('#modalBody').text('¿Desea eliminar la venta '+id_venta+' del cliente '+cliente+'?');
    $('#deleteClienteModal').modal('show');
  }
  else
  {
    alert('Seleccione un cliente de la tabla');
  }
});




$('#delCliente').click(function(){

  $.post('/sistema/venta/eliminar',{id:id_venta},function(data){

    
    if(data.ok)
    {
      
      $('#deleteClienteModal').modal('hide');
      location.reload();
    }
    else{
      alert('No se pudo eliminar el registro.');
    }
  });
});

