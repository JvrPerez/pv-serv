var id_cliente=0;
var nombre='';
var telefono='';
var correo='';
var rfc='';
var direccion='';
var ciudad='';


function tableLoad(){
  var table = $('#tableClientes').DataTable({
    "lengthMenu": [[5,10, 25, 50,100], [5,10, 25, 50,100]],
    language: {
      url:'/vendor/datatables/Spanish.json',
      select: {
        rows: ""
      }
      // buttons: {colvis: 'Cambiar columnas'}
    },

    select:{style: 'single'}
      // dom: 'Bfrtip',
      // stateSave: true,
      // buttons: [{
      //   collectionLayout: 'fixed two-column',
      //   extend: 'colvis',
      // }],

  });
  
  table
  .on('select',function(e, dt, type, index){
    var rowData = table.rows(index).data().toArray();

    id_cliente=rowData[0][0],
    nombre=rowData[0][1],
    telefono=rowData[0][2],
    correo=rowData[0][3],
    rfc=rowData[0][8],
    direccion=rowData[0][9],
    ciudad = rowData[0][10]
  

  })
  .on('deselect',function(){
    id_cliente=0;
  });
}


$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});


  tableLoad();

  $('#addCliente').submit(function(e){

    e.preventDefault();
  
    var url = $(this).attr('action');
    var cont = $(this).serialize();
  
  
            $.post(url,cont,function(data){
              alert(data.result);
              $('#aditClienteModal').modal('hide');
              $('#editClienteModal').on('hidden.bs.modal', function () {
                $('#tableCont').load(' #tableCont');
              
              })
             
            },'json');
  
        
  });

$('#btnedit').click(function(){

  if(id_cliente!=0)
  {
    $('#idcliente').val(id_cliente);
    $('#cnombre').val(nombre);
    $('#crfc').val(rfc);
    $('#ccorreo').val(correo);
    $('#ctelefono').val(telefono);
    $('#cdireccion').val(direccion);
    $('#cciudad').val(ciudad);
    $('#editClienteModal').modal('show');
  }
  else
  {
    alert('Seleccione un cliente de la tabla');
  }


});

$('#btndelete').click(function(){

  if(id_cliente!=0)
  {
    $('#modalBody').text('¿Desea eliminar a '+nombre+' de su lista de clientes?');
    $('#deleteClienteModal').modal('show');
  }
  else
  {
    alert('Seleccione un cliente de la tabla');
  }
});


$('#editCliente').submit(function(e){

  e.preventDefault();

  var url = $(this).attr('action');
  var cont = $(this).serialize();


          $.post(url,cont,function(data){
            alert(data.result);
            if(data.ok)
            {
              
              $('#editClienteModal').modal('hide');
              $('#tableCont').load(' #tableCont',function (){
                tableLoad();
            });
            }
            else{
              alert('No se pudo actualizar el registro.');
            }
           
          },'json');

      
});

$('#delCliente').click(function(){

  $.post('/sistema/cliente/eliminar',{id:id_cliente},function(data){

    
    if(data.ok)
    {
      
      $('#deleteClienteModal').modal('hide');
      $('#tableCont').load(' #tableCont',function (){
        tableLoad();
    });
    }
    else{
      alert('No se pudo eliminar el registro.');
    }
  });
});