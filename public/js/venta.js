

$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

var idprod=0;
var Gtotal=0;
var Subtotal=0;
var Descuento=0;
var idCliente=0;
var Gproducto='';
var idproducto=0;
var idServicio=0;
var pStock=0;
var pStockEnable;
/*---------------------------*/

function select(){
$("#select-cliente").select2({
    language: 'es',
    placeholder:'',
    allowClear:true,
    width:'100%'
  });

  $('#select-cliente').on('select2:select', function (e) {
    var cliente = e.params.data;
    idCliente=cliente.id;
    $.post('/sistema/cliente',{id:cliente.id},function (data) { 

          $('#detCorreo').val(data.correo);
          $('#detTelefono').val(data.telefono);
          $('#detRfc').val(data.rfc);
          $('#detDireccion').val(data.direccion);
          $('#detCiudad').val(data.ciudad);

          $('#detCliente').show('slow');

     },'json');
  });

  $('#select-cliente').on('select2:unselect',function(e){
    idCliente=0;
    $('#detCliente').hide('slow');
  });



  $("#select-articulo").select2({

    language: 'es',
    placeholder:'',
    allowClear:true,
    width:'100%'
  });


  $("#bCliente").select2({
    language: 'es',
    placeholder:'Consultar con nombre del cliente',
    allowClear:true,
    width:'100%'
  });
  
  
  $('#select-articulo').on('select2:select',function(e){
    var producto= e.params.data;
    idproducto=producto.id;
    Gproducto=producto.text;
    $.post('/sistema/showProducto',{id:idproducto},function(data){
      pStock= parseInt(data.stock);
      pStockEnable=data.enable;
      $('#precio').val(data.result);
  
    },'json');
  
  });
  
  $('#select-articulo').on('select2:unselect',function(){
    Gstock=0;
    $('#precio').val('');
  });
  
  $('#select-cliente').on('select2:unselect',function(e){
    idCliente=0;
 
    $('#detCliente').hide('slow');
  });

}

select();



$('.cantidad').TouchSpin({
  initval: 1,
  min:1
});

$('#check').on('change',function(){

  if ($(this).prop('checked')) {
    $('#stock').prop('disabled',false);
  } else {
    $('#stock').prop('disabled',true);
  }

});

// $('input[name=enable]').click(function(){

//   if ($('#efolio').prop('checked')) {

//     $('#inputCliente').hide('slow',function () {
//         $('#inputFolio').show('slow');
//       });

//   } else {
    
//     $('#inputFolio').hide('slow',function() {
//       $('#inputCliente').show('slow');
//       });
//   }

// });






/*------------------- VENTA ----------------------*/

$('#addCliente').submit(function(e){

  e.preventDefault();

  var url = $(this).attr('action');
  var cont = $(this).serialize();


  $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });

          $.post(url,cont,function(data){
            alert(data.result);
            $('#addCancel').click();
            $('#addClienteModal').on('hidden.bs.modal', function (e) {
              $('#clientelist').load(' #clientelist',function(){

                select();
              });
              
         
            })
           
          },'json');

      
});


$('#addClienteModal').on('hidden.bs.modal', function (e) {
    $('#addCliente')[0].reset();
  });

  $('#cobrarModal').on('hidden.bs.modal', function (e) {
    $('#recibo').val('');
    $('#cambio').val('');
  });

  $('#btnCobrarModal').click(function(){
    if(idCliente){$('#cobrarModal').modal('show');}else{alert('DEBE ELEGIR UN CLIENTE PRIMERO');}
  });
/*----------------------------------*/


function idProd(id) {
  idprod=id;
  $('#modCantidad').val($('#tagCantidad'+idprod).text());
}


function modCantidad(){


  var cantidad = $('#modCantidad').val();
  var precio = $('#tagPrecio'+idprod).text();
  var importe = '<i class="fa fa-fw fa-dollar"></i>'+(cantidad*precio).toFixed(2);

  document.getElementById('tagCantidad'+idprod).innerHTML=cantidad;
  document.getElementById('tagImporte'+idprod).innerHTML=importe;
  total();
  $('#addCantidadModal').modal('hide');
  
}
/*----------------------------------*/

function addProducto()
{
  var cant = $('#cantidad').val();
  var pre = $('#precio').val();
  var imp = pre*cant;

  if(Gproducto && pre)
  {
    if(pStockEnable)
        if(cant<=pStock)
        {
          var id = '<tr id="'+idproducto+'" ><th class="hidden p-0 border-top-0" style="font-size:3px;" scope="row">'+idproducto+'</th><td class="hidden p-0 border-top-0"></td>';
          var nombre = '<td class="text-left">'+Gproducto+'</td>';
          var cantidad = '<td><button onclick="idProd('+idproducto+')" data-toggle="modal" data-target="#addCantidadModal" type="button" class="btn btn-primary p-1"><span id="tagCantidad'+idproducto+'" style="font-size:15px;" class="badge">'+cant+'</span></button></td>';
          var precio = '<td id="tagPrecio'+idproducto+'" ><i class="fa fa-fw fa-dollar"></i>'+parseFloat(pre).toFixed(2)+'</td>';
          var importe = '<td id="tagImporte'+idproducto+'" class="importe" ><i class="fa fa-fw fa-dollar"></i>'+(imp).toFixed(2)+'</td>';
          var borrar = '<td><button type="button" onclick="remove('+idproducto+')" class="btn btn-danger p-0 px-1"><i class="fa fa-fw fa-times-circle"></i></button></td></tr>';
          
          $('#addProducto').append(id+nombre+cantidad+precio+importe+borrar);
        
          total();
          
          $('#cantidad').val(1);
          // $('#precio').val('');
          // Gproducto='';
        }
        else
        {
          alert('Stock insuficiente. Solo hay '+pStock+' disponibles');
        }
    else
    {
      var id = '<tr id="'+idproducto+'" ><th class="hidden p-0 border-top-0" style="font-size:3px;" scope="row">'+idproducto+'</th><td class="hidden p-0 border-top-0"></td>';
          var nombre = '<td class="text-left">'+Gproducto+'</td>';
          var cantidad = '<td><button onclick="idProd('+idproducto+')" data-toggle="modal" data-target="#addCantidadModal" type="button" class="btn btn-primary p-1"><span id="tagCantidad'+idproducto+'" style="font-size:15px;" class="badge">'+cant+'</span></button></td>';
          var precio = '<td id="tagPrecio'+idproducto+'" ><i class="fa fa-fw fa-dollar"></i>'+parseFloat(pre).toFixed(2)+'</td>';
          var importe = '<td id="tagImporte'+idproducto+'" class="importe" ><i class="fa fa-fw fa-dollar"></i>'+(imp).toFixed(2)+'</td>';
          var borrar = '<td><button type="button" onclick="remove('+idproducto+')" class="btn btn-danger p-0 px-1"><i class="fa fa-fw fa-times-circle"></i></button></td></tr>';
          
          $('#addProducto').append(id+nombre+cantidad+precio+importe+borrar);
        
          total();
          
          $('#cantidad').val(1);
          // $('#precio').val('');
          // Gproducto='';
    }
  }
  else{
    alert('Los campos "Producto" o "Precio" estan vacios');
  }


}
/*----------------------*/
function remove(id)
{
  $('#'+id).remove();
  total();
  
}



/*----------------------------------------*/
$('#recibo').on('keyup',function(){

  var recibo = parseFloat($('#recibo').val());
  var cambio = recibo-Gtotal;
  if(cambio>=0)
  {
    $('#cambio').val((cambio).toFixed(2));
  }
  else
  {
    $('#cambio').val('');
  }

});

  $('#descuento').on('keyup',function(){
    Descuento=$(this).val();
    Descuento = parseFloat(Descuento);
    if(Gtotal>Descuento)
    {
      total();
      // Gtotal=Gtotal-Descuento;
      // document.getElementById('total').innerHTML='<i class="fa fa-fw fa-dollar"></i>'+Gtotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }
    else
    {
      Descuento = 0;
      total();
    }

  });

  function total(){

    var total=0;
    $('.importe').each(function(){

      total = total+ parseFloat($(this).text());

    });
 
    Subtotal = total;
    Gtotal = total-Descuento;
    

    document.getElementById('total').innerHTML='<i class="fa fa-fw fa-dollar"></i>'+Gtotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    document.getElementById('subtotal').innerHTML='<i class="fa fa-fw fa-dollar"></i>'+Subtotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    document.getElementById('modTotal').innerHTML='Total<i class="fa fa-fw fa-dollar"></i>'+Gtotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    

  }


  $('#vender').click( function() {

    var table = $('#detVenta').tableToJSON({
      ignoreHiddenRows:false
    });

    $.post('/sistema/venta',{cliente:idCliente,detVenta:table,total:Gtotal,subtotal:Subtotal,descuento:Descuento},function(data){
      window.open('/sistema/nota/'+data.result);
      $('#cobrarModal').modal('hide');
    },'json');

  });

  $('.numeros').on('keyup',function(){
    var val= $(this).val();
    var test = /^([a-zA-zñ<>{}´+'¿?-]+)?$/
    if(test.test(String(val)))
    {
      $(this).val('');
    }
  });

  function removeServ(id)
  {
    $('#serv'+id).remove();
    total();
    
  }

  $('#buscaFolio').click(function (e) {
    e.preventDefault;
    var id = $('#idFolio').val();

    $.post('/sistema/serviciofolio',{id:id},function(data){
      
      if(data.result)
      {
        var result = '<tr><td><b>Folio: </b>'+("000000" + data.id).slice(-6)+'</td><td><b>Registrado: </b>'+data.fecha+'</td></tr>'+              
                    '<tr><td><b>Cliente: </b>'+data.cliente+'</td></tr>'+
                    '<tr><td><b>Tipo: </b>'+data.tipo+'</td></tr>'+
                    '<tr><td><b>Marca: </b>'+data.marca+'</td><td><b>Modelo: </b>'+data.modelo+'</td></tr>';

                    document.getElementById('resultado').innerHTML=result;
                    document.getElementById('descrip').innerHTML='<b>Descripción: </b>'+data.descripcion.replace(/\r?\n/g, " - ");
      }
      else
      {
        $('#descrip').empty();
        document.getElementById('resultado').innerHTML='<p class="w-100 text-center">No se encontraron resultados</p>';
      }
    },'json');
    });



    $('#agregaServ').click(function (e) {
      e.preventDefault;
      var id = $('#idFolio').val();
      var valor = $('#servPrecio').val();
      if(valor===''){ alert('Especifique un precio'); return;}

      $.post('/sistema/serviciofolio',{id:id},function(data){
        
        if(data.result)
        {
          

          var id = '<tr id="serv'+data.id+'" ><td class="hidden p-0 border-top-0"></td><td class="hidden p-0 border-top-0" style="font-size:3px;" scope="row">'+data.id+'</td>';
          var nombre = '<td class="text-left">'+data.tipo+'</td>';
          var cantidad = '<td><button type="button" class="btn btn-secondary p-1"><span style="font-size:15px;" class="badge">1</span></button></td>';
          var precio = '<td><i class="fa fa-fw fa-dollar"></i>'+parseFloat(valor).toFixed(2)+'</td>';
          var importe = '<td class="importe" ><i class="fa fa-fw fa-dollar"></i>'+parseFloat(valor).toFixed(2)+'</td>';
          var borrar = '<td><button type="button" onclick="removeServ('+data.id+')" class="btn btn-danger p-0 px-1"><i class="fa fa-fw fa-times-circle"></i></button></td></tr>';
          
          $('#addProducto').append(id+nombre+cantidad+precio+importe+borrar);
        
          total();
          
        }
        else
        {
          alert('El folio '+("000000" + parseInt(id)).slice(-6)+' no se encuentra registrado');
        }
      },'json');
      });






    $('#bCliente').on('select2:select',function(e){
      var cliente= e.params.data;
      var result='';
      $.post('/sistema/serviciocliente',{id:cliente.id},function(data){

        if(Object.keys(data).length != 0)
        {
          $('#resultado').empty();
          $('#descrip').empty();
          var terminado='';
          $.each(data, function (ind, elem) { 
            if(elem.terminado!=null){terminado=elem.terminado}else{terminado='Aun no se valida como terminado'}

            result = '<tr><td><b>Folio: </b>'+("000000" + elem.id).slice(-6)+'</td><td><b>Registrado: </b>'+elem.created_at+'</td></tr>'+
                  '<tr><td colspan="2"><b>Terminado: </b>'+terminado+'<td></tr>'+              
                  '<tr><td><b>Cliente: </b>'+cliente.text+'</td></tr>'+
                  '<tr><td><b>Tipo: </b>'+elem.tipo+'</td></tr>'+
                  '<tr class="border-bottom border-dark"><td><b>Marca: </b>'+elem.marca+'</td><td><b>Modelo: </b>'+elem.modelo+'</td></tr>';

                  $('#resultado').append(result);
          }); 

        }
        else
        {
          document.getElementById('resultado').innerHTML='<p class="w-100 text-center">No se encontraron resultados</p>';
        }

        
      },'json');
    
    });


    $('#InveForm').submit(function(e){

      e.preventDefault();
    
      var url = $(this).attr('action');
      var cont = $(this).serialize();
      console.log(cont);
              $.post(url,cont,function(data){
                alert(data.result);

                $('#productoModal').on('hidden.bs.modal', function (e) {
                  $('#invlist').load(' #invlist',function(){
                    select();
                  });
     
                });
               
              },'json');
    
          
    });