
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  var id_servicio=0;
  var terminado='';
  
  
  function tableLoad(){
    var table = $('#tableClientes').DataTable({
      "lengthMenu": [[5,10, 25, 50,100], [5,10, 25, 50,100]],
      language: {
        url:'/vendor/datatables/Spanish.json',
        select: {
          rows: ""
        }
        // buttons: {colvis: 'Cambiar columnas'}
      },
  
      select:{style: 'single'}
        // dom: 'Bfrtip',
        // stateSave: true,
        // buttons: [{
        //   collectionLayout: 'fixed two-column',
        //   extend: 'colvis',
        // }],
  
    });
    
    table
    .on('select',function(e, dt, type, index){
      var rowData = table.rows(index).data().toArray();
  
      id_servicio=rowData[0][0],
      cliente = rowData[0][1],
      terminado=rowData[0][6]
  
    })
    .on('deselect',function(){
      id_servicio=0;
      terminado='';
    });
  }
  
  
  
  
    tableLoad();
  
  
  $('#btncomprobante').click(function(){
  
    if(id_servicio!=0)
    {
      window.open('/sistema/comprobante/'+id_servicio);
    }
    else
    {
      alert('Seleccione un cliente de la tabla');
    }
  
  
  });

  $('#termServ').click(function(){
    
    $.post('/sistema/servicio/terminado',{id:id_servicio},function(data){
  
      
        if(data.ok)
        {
          
          $('#termServModal').modal('hide');
          $('#tableCont').load(' #tableCont',function (){
            tableLoad();
        });
        }
        else{
          alert('No se pudo modificar el registro.');
        }
      });
  });
  
  
  $('#btnTerminado').click(function(){
  
    if(id_servicio!=0)
    {
      if(terminado === 'No')
      {
        $('#modalBodyServ').text('¿Desea marcar el servicio '+id_servicio+' del cliente '+cliente+' como TERMINADO ?');
        $('#termServModal').modal('show');
      }
      else
      {
        $('#modalBodyServ').text('¿Desea marcar el servicio '+id_servicio+' del cliente '+cliente+' como NO TERMINADO ?');
        $('#termServModal').modal('show');
      }

    }
    else
    {
      alert('Seleccione un cliente de la tabla');
    }
  });

  
  $('#btndeleteservicio').click(function(){
  
    if(id_servicio!=0)
    {
      $('#modalBody').text('¿Desea eliminar el servicio '+id_servicio+' del cliente '+cliente+'?');
      $('#deleteClienteModal').modal('show');
    }
    else
    {
      alert('Seleccione un cliente de la tabla');
    }
  });
  
  
  
  
  $('#delCliente').click(function(){
  
    $.post('/sistema/servicio/eliminar',{id:id_servicio},function(data){
  
      
      if(data.ok)
      {
        
        $('#deleteClienteModal').modal('hide');
        $('#tableCont').load(' #tableCont',function (){
          tableLoad();
      });
      }
      else{
        alert('No se pudo eliminar el registro.');
      }
    });
  });