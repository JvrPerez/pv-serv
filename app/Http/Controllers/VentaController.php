<?php

namespace App\Http\Controllers;

use App\Clases\NumeroALetras as Letras;
use DateTime;
use App\Servicio;
use App\Venta;
use App\Cliente;
use App\Inventario;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use PDF;

class VentaController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	} 
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $venta = Venta::whereDate('created_at', date('Y-m-d') )->get();

        return view('consultas.ventas')->with('ventas',$venta);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $venta = Venta::create([
            'user_id' => Auth::user()->id,
            'cliente_id' => $request->cliente,
            'subtotal' => $request->subtotal,
            'descuento' => $request->descuento,
            'total' => $request->total

        ]);

        $detventa = $request->detVenta;

        foreach ($detventa as $item) {
            
            if( $item['IN']!=null)
            {
                $inv = Inventario::find($item['IN']);
                if($inv->enable)
                {
                 $stock =$inv->stock;
                 $inv->stock = $stock - (int)$item['Cantidad'];
                 $inv->save();
                }
            }

            $venta->inventarios()->attach($item['IN'],['cantidad'=>$item['Cantidad'],'importe'=>$item['Importe']]);
                
            $venta->servicios()->attach($item['SV'],['cantidad'=>$item['Cantidad'],'importe'=>$item['Importe']]);

            if($item['SV']!=null)
            {
                $serv = Servicio::find($item['SV']);
                $date = new DateTime;
    
                $serv->terminado=$date->format('Y-m-d H:i:s');
                $serv->save();
            }

        

            // $dventa = new Detalle_venta();
            // $dventa->venta_id=$venta->id; 
            // $dventa->descripcion = $item['Producto'];
            // $dventa->cantidad = $item['Cantidad'];
            // $dventa->precio = $item['Precio'];
            // $dventa->importe = $item['Importe'];
            // $dventa->save();
        }

        return response()->json([
            'result' => $venta->id
        ]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function show(Venta $venta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function edit(Venta $venta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Venta $venta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Venta $venta)
    {
        //
    }

    

    public function eliminar (Request $request)
    {
        $venta = Venta::find($request->id);
        $venta->delete();

        return response()->json([
            'ok' => true
        ]);
    }

    public function consultaVentaG()
    {
        $venta= Venta::all();

        return view('consultas.general.gventas')->with('ventas',$venta);
    }

    public function consultaVentaF(Request $request)
    {
        $venta = Venta::where('created_at','>=',$request->inicio)
        ->where('created_at','<=',$request->final)->get();

        return view('consultas.general.fventas')->with('ventas',$venta);
    }

    public function consultaFecha ()
    {
        return view('consultas.general.fventas');
    }
}
