<?php

namespace App\Http\Controllers;

use App\Inventario;
use App\Categoria;
use Illuminate\Http\Request;

class InventarioController extends Controller
{

    public function __construct()
	{
		$this->middleware('auth');
	} 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cat = Categoria::all();
        $inventario = Inventario::all();

        return view('consultas.inventario')
        ->with('inventario',$inventario)
        ->with('categoria',$cat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
        $inv = new Inventario();
        $inv->fill($request->all());
        $inv->save();
        
        return response()->json([
            'result' => 'Producto agregado'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function show(Inventario $inventario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function editInventario(Request $request)
    {
        $inv = Inventario::find($request->id);

        $inv->fill($request->all());
        $inv->save();

        return response()->json([
            'result' => 'Registro actualizado'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventario $inventario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function eliminar(Request $request)
    {
        $inv = Inventario::find($request->id);
        $inv->delete();

        return response()->json([
            'ok'=>true,
            'result' => 'Registro eliminado'
        ]);
    }

    public function showProducto(Request $request)
    {
        $prod = Inventario::find($request->id);

        return response()->json([
            'enable'=>$prod->enable,
            'stock' => $prod->stock,
            'result'=>$prod->preciov
        ]);
    }
}
