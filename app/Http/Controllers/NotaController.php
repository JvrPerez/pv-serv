<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clases\NumeroALetras as Letras;
use DateTime;
use App\Servicio;
use App\Venta;
use PDF;

class NotaController extends Controller
{
    public function comprobante ($id)
    {
        $serv = Servicio::find($id);

        $nota = view('sistema.comprobante')
        ->with('servicio',$serv);
      
        $pdf = PDF::setPaper('letter')
        ->loadHTML($nota)
        ->setOptions( ['defaultFont' => 'sans-serif']);
        return $pdf->stream();
    }

    public function nota($id)
    {
        // // return view('sistema.nota');
        // $letra = new Letras();
        // $venta = Venta::find($id);
        // $detventa = Detalle_venta::where('venta_id',$id)->get();
        // $nota = view('sistema.nota')
        // ->with('venta',$venta)
        // ->with('letras',$letra)
        // ->with('detventa',$detventa);
      
        // $pdf = PDF::setPaper('letter','landscape')
        // ->loadHTML($nota)
        // ->setOptions( ['defaultFont' => 'sans-serif']);
        // return $pdf->stream();


                $letra = new Letras();
                $venta = Venta::find($id);
                $nota = view('sistema.nota')
                ->with('venta',$venta)
                ->with('letras',$letra);
              
                $pdf = PDF::setPaper('letter','landscape')
                ->loadHTML($nota)
                ->setOptions( ['defaultFont' => 'sans-serif']);
                return $pdf->stream();
    }
}
