<?php

namespace App\Http\Controllers;

use App\Cliente;
use Carbon\Carbon;
use Illuminate\Support\MessageBag;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	} 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cliente = Cliente::all();
        return view('consultas.clientes')->with('cliente',$cliente);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
     {  
        $validator = Validator::make($request->all(), [
            'nombre' => 'regex:/(^([a-zñáéíóú A-ZÑÁÉÍÓÚ]+)*?$)/um|required|string|max:255',
            'correo' => 'nullable|unique:clientes|string|email',
            'rfc' => 'nullable|string|unique:clientes',
            // 'telefono'=> 'nullable|regex:/(^([\d]{7}|[\d]{10})$)/u|unique:clientes'
        ]);

        if ($validator->fails()) {

            $errors= $validator->errors();
            if($errors->first('correo'))
            {
                return response()->json([
                    'result'=> $errors->first('correo')
                    ]);
            }

            if($errors->first('nombre'))
            {
                return response()->json([
                    'result'=> $errors->first('nombre')
                    ]);
            }

            if($errors->first('rfc'))
            {
                return response()->json([
                    'result'=> $errors->first('rfc')
                    ]);
            }
            
            // if($errors->first('telefono'))
            // {
            //     return response()->json([
            //         'result'=> $errors->first('telefono')
            //         ]);
            // }
            
       
 
        }
        else{
            $cliente = new Cliente();

        $cliente->nombre=$request->nombre;
        $cliente->rfc=$request->rfc;
        $cliente->correo=$request->correo;
        $cliente->telefono=$request->telefono;
        $cliente->ciudad = $request->ciudad;
        $cliente->direccion= $request->direccion;

        $cliente->save();
        return response()->json([
            'result'=>'Cliente agregado'
        ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        
    }

    public function showCliente(Request $request)
    {

       $cliente= Cliente::find($request->id);

       return response()->json([
            'correo'=>$cliente->correo,
            'telefono' => $cliente->telefono,
            'rfc' => $cliente->rfc,
            'direccion' => $cliente->direccion,
            'ciudad' =>$cliente->ciudad
       ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function editar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'regex:/(^([a-zñáéíóú A-ZÑÁÉÍÓÚ]+)*?$)/um|required|string|max:255',
            'correo' => 'nullable|unique:clientes|string|email',
            'rfc' => 'nullable|string|unique:clientes',
            // 'telefono'=> 'nullable|regex:/(^([\d]{7}|[\d]{10})$)/u|unique:clientes'
        ]);

        if ($validator->fails()) {

            $errors= $validator->errors();
            if($errors->first('correo'))
            {
                return response()->json([
                    'result'=> $errors->first('correo')
                    ]);
            }

            if($errors->first('nombre'))
            {
                return response()->json([
                    'result'=> $errors->first('nombre')
                    ]);
            }

            if($errors->first('rfc'))
            {
                return response()->json([
                    'result'=> $errors->first('rfc')
                    ]);
            }
            
            // if($errors->first('telefono'))
            // {
            //     return response()->json([
            //         'result'=> $errors->first('telefono')
            //         ]);
            // }
            
       
 
        }
        else{

            $cliente = Cliente::find($request->id);

        $cliente->nombre=$request->nombre;
        $cliente->rfc=$request->rfc;
        $cliente->correo=$request->correo;
        $cliente->telefono=$request->telefono;
        $cliente->ciudad = $request->ciudad;
        $cliente->direccion= $request->direccion;

        $cliente->save();
        return response()->json([
            'result'=>'Cliente modificado',
            'ok'=>true
        ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function eliminar(Request $request)
    {
        $cliente = Cliente::find($request->id);
        $cliente->delete();

        return response()->json([
            'ok'=>true
        ]);
    }

    public function consultaClienteF(Request $request)
    {
        // $sql = "SELECT c.*,SUM(v.total) as 'total',count(v.total) as 'ventas'
        // FROM clientes as c INNER JOIN ventas as v ON c.id = v.cliente_id
        // WHERE v.created_at between ? AND ?
        // GROUP BY c.nombre;";
$final = new Carbon($request->final);
$final2 =$final;
$inicio =new Carbon($request->inicio);

        $cliente = Cliente::select('clientes.*',DB::raw('SUM(ventas.descuento) as descuento'),DB::raw('SUM(ventas.total) as total'),DB::raw('COUNT(ventas.id) as ventas'))
        ->join('ventas','clientes.id','=','ventas.cliente_id')
        ->whereBetween('ventas.created_at',[$inicio,$final->addDay()])
        ->groupBy('clientes.nombre')
        ->get();


   
        return view('consultas.fclientes')
        ->with('cliente',$cliente)
        ->with('inicio',$inicio)
        ->with('final2',$final2);
    }

    public function consultaFecha ()
    {
        return view('consultas.fclientes');
    }
}
