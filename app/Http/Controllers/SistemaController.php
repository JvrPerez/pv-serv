<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cliente;
use App\Inventario;

class SistemaController extends Controller
{
    public function index()
    {
        if(Auth::check())
        {
            if (Auth::user()->tipo == "admin") {
                $cliente = Cliente::orderBy('nombre','desc')->get();
                $producto = Inventario::orderBy('id','desc')->get();
                return view('sistema.venta')
                ->with('cliente',$cliente)
                ->with('productos',$producto);
            } else {
                return view('errors.access');
            }
        }else {
            return view('auth.login');
        }
   
        
    }
}
