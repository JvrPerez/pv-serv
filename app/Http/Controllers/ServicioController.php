<?php

namespace App\Http\Controllers;

use PDF;
use App\Servicio;
use App\Cliente;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ServicioController extends Controller
{

    public function __construct()
	{
		$this->middleware('auth');
	} 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check())
        {
            if (Auth::user()->tipo == "admin") {
                $cliente = Cliente::orderBy('nombre','desc')->get();

            return view('sistema.servicio')
            ->with('cliente',$cliente);
            } 
            else {
                return view('errors.access');
            }
        }else {
            return view('sistema.login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $serv = Servicio::create([
            'user_id' => Auth::user()->id,
            'cliente_id' => $request->cliente,
            'marca' => $request->marca,
            'modelo' => $request->modelo,
            'serial' => $request->serial,
            'tipo' => $request->tipo,
            'descripcion' => $request->descripcion,
            'observaciones' => $request->observaciones

        ]);

        return response()->json([
            'result' => $serv->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function show(Servicio $servicio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function edit(Servicio $servicio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Servicio $servicio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Servicio $servicio)
    {
        //
    }



    public function buscarFolio(Request $request)
    {
        $serv = Servicio::find($request->id);

        if(!$serv)
        {
            return response()->json(['result' => false]);
        }
        else
        {
            return response()->json([
                'result' => true,
                'tipo' => $serv->tipo,
                'marca' => $serv->marca,
                'modelo' => $serv->modelo,
                'id'=> $serv->id,
                'fecha' => date($serv->created_at),
                'cliente' => $serv->cliente->nombre,
                'descripcion' => $serv->descripcion
            ]);
        }
    }

    public function buscarCliente(Request $request)
    {
        $servicio = Servicio::where('cliente_id',$request->id)->latest()->limit(3)->get();

        return response()->json($servicio);
    }

    public function recientes()
    {
        $serv = Servicio::whereDate('created_at', date('Y-m-d') )->get();
        return view('consultas.servicios')->with('servicio',$serv);
    }

    public function eliminar(Request $request)
    {
        $serv=Servicio::find($request->id);
        $serv->delete();

        return response()->json([
            'ok' => true
        ]);
    }

    public function terminado (Request $request)
    {
        $serv = Servicio::find($request->id);

        if($serv->terminado==null)
        {
            $date = new DateTime;
            $serv->terminado=$date->format('Y-m-d H:i:s');
            $serv->save();
    
            return response()->json([
                'ok' => true
            ]);
        }
        else
        {
            $serv->terminado=null;
            $serv->save();
    
            return response()->json([
                'ok' => true
            ]);
        }
        


    }

    public function consultaServicioG()
    {
        $serv= Servicio::all();

        return view('consultas.general.gservicios')->with('servicio',$serv);
    }

    public function consultaServicioF(Request $request)
    {
        $serv = Servicio::where('created_at','>=',$request->inicio)
        ->where('created_at','<=',$request->final)->get();

        return view('consultas.general.fservicio')->with('servicio',$serv);
    }

    public function consultaFecha ()
    {
        return view('consultas.general.fservicio');
    }
}
