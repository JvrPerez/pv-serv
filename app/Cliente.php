<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';

    protected $fillable = [
        'id', 'nombre', 'apellido','rfc','correo','telefono'.'direccion','ciudad'
    ];

    public function servicios()
    {
        return $this->hasMany('App\Servicio');
    }

    public function ventas()
    {
        return $this->hasMany('App\Venta');
    }
}
