<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table='categorias';

    protected $fillable = [
        'id', 'nombre',
    ];

    public function inventarios()
    {
        return $this->hasMany('App\Inventario','categoria_id');
    }

}
