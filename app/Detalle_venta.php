<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_venta extends Model
{
    protected $table = 'detalle_venta';

    protected $fillable = [
        'id', 'venta_id', 'servicio_id', 'cantidad', 'precio', 'descripcion', 'importe'
    ];


}
