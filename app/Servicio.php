<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'servicios';

    protected $fillable = [
        'id','user_id', 'cliente_id','marca','modelo','serial','terminado','tipo', 
        'descripcion', 'observaciones'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }

    public function ventas()
    {
        return $this->belongsToMany('App\Venta','inventarios_servicios_ventas')->withPivot('inventario_id','cantidad','importe')->withTimeStamp();
    }

    public function inventarios()
    {
        return $this->belongsToMany('App\Inventario','inventarios_servicios_ventas')->withPivot('venta_id','cantidad','importe')->withTimeStamp();
    }
}
