<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $table = 'ventas';

    protected $fillable = [
        'id', 'user_id', 'cliente_id', 'subtotal', 'descuento', 'total'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }

    public function inventarios()
    {
        return $this->belongsToMany('App\Inventario','inventarios_servicios_ventas')->withTimestamps()->withPivot('servicio_id','cantidad','importe');
    }

    public function servicios()
    {
        return $this->belongsToMany('App\Servicio','inventarios_servicios_ventas')->withTimestamps()->withPivot('inventario_id','cantidad','importe');
    }
}
