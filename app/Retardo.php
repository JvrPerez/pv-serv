<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retardo extends Model
{
    protected $table='retardos';

    protected $fillable = [
        'id', 'dias', 'valor', 'enable'
    ];
}
