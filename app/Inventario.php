<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    protected $table = 'inventarios';

    protected $fillable = [
        'categoria_id','descripcion', 'stock','enable','precioc','preciov', 'created_at',
    ];

    public function ventas()
    {
        return $this->belongsToMany('App\Venta','inventarios')->withTimestamps();
    }

    public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }

}
