<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InventariosServiciosVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventarios_servicios_ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('venta_id')->unsigned()->nullable();
            $table->integer('inventario_id')->unsigned()->nullable();
            $table->integer('servicio_id')->unsigned()->nullable();
            $table->integer('cantidad')->unsigned();
            $table->float('importe',8,2);
            $table->foreign('venta_id')->references('id')->on('ventas');
            $table->foreign('inventario_id')->references('id')->on('inventarios');
            $table->foreign('servicio_id')->references('id')->on('servicios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
