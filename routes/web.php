<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'sistema'], function () {
    
    Route::resource('Cliente', 'ClienteController');
    Route::resource('/venta', 'VentaController');
    Route::post('/venta/eliminar','VentaController@eliminar');

    Route::get('/nota/{id}', 'NotaController@nota')->name('venta.nota');
    Route::get('/','SistemaController@index')->name('sistema');
    Route::post('/cliente','ClienteController@showCliente');
    Route::post('/cliente/editar','ClienteController@editar')->name('cliente.editar');
    Route::post('/cliente/eliminar','ClienteController@eliminar');
    Route::resource('inventario','InventarioController');
    Route::post('/showProducto','InventarioController@showProducto');
    Route::resource('servicio','ServicioController');
    Route::post('serviciofolio','ServicioController@buscarFolio');
    Route::post('serviciocliente','ServicioController@buscarCliente');
    Route::get('/recientes/servicio','ServicioController@recientes')->name('servicio.recientes');
    Route::get('/comprobante/{id}','NotaController@comprobante')->name('servicio.comprobante');
    Route::post('/servicio/eliminar','ServicioController@eliminar');
    Route::post('/servicio/terminado','ServicioController@terminado');

    Route::get('/servicios/general','ServicioController@consultaServicioG')->name('servicio.gconsulta');
    Route::get('/servicios/fecha','ServicioController@consultaFecha')->name('servicio.fecha');
    Route::post('/serviciopost/fecha','ServicioController@consultaServicioF')->name('servicio.fconsulta');

    Route::get('/ventas/general','VentaController@consultaVentaG')->name('venta.gconsulta');
    Route::get('/ventas/fecha','VentaController@consultaFecha')->name('venta.fecha');
    Route::post('/ventapost/fecha','VentaController@consultaVentaF')->name('venta.fconsulta');
    
    Route::get('/clientes/fecha','ClienteController@consultaFecha')->name('cliente.fecha');
    Route::post('/clientepost/fecha','ClienteController@consultaClienteF')->name('cliente.fconsulta');
    
    Route::post('/inventario/update','InventarioController@editInventario')->name('inventario.edit');
    Route::post('/inventario/delete','InventarioController@eliminar');

    Route::resource('categoria','CategoriaController');
    Route::post('/categoria/update','CategoriaController@editCategoria')->name('categoria.edit');
    Route::post('/categoria/delete','CategoriaController@eliminar');
});


